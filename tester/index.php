<?php

use Anytimestream\UI\Assets\CSSAsset;
use Anytimestream\UI\Assets\JavaScriptAsset;
use Anytimestream\UI\SimplePage;
use Anytimestream\UI\Tags;

require_once 'config.php';
require_once 'autoloader.php';
require_once 'test.php';

$page = new SimplePage("Testing Forms");
$page->getHead()->add(new CSSAsset("css/bootstrap.min.css"));
$page->getHead()->add(new CSSAsset("../src/Anytimestream/Assets/css/ats-ui.css"));
$container = $page->createContainer(Tags::DIV);
$container->addAttribute("style", "margin-top: 30px; width: 600px;");
$container->setContent($form);
$page->getContents()->add($container);
$page->getBody()->add(new JavaScriptAsset("js/jquery.min.js"));
$page->getBody()->add(new JavaScriptAsset("js/bootstrap.min.js"));
$page->render();

