<?php

use Anytimestream\UI\ColumnTypes;
use Anytimestream\UI\Form;
use Anytimestream\UI\Forms\Button;
use Anytimestream\UI\Forms\ButtonTypes;
use Anytimestream\UI\Forms\Inputs\Checkbox;
use Anytimestream\UI\Forms\Inputs\DateField;
use Anytimestream\UI\Forms\Inputs\DropdownList;
use Anytimestream\UI\Forms\Inputs\FileUpload;
use Anytimestream\UI\Forms\Inputs\TextArea;
use Anytimestream\UI\Forms\Inputs\TextField;
use Anytimestream\UI\Forms\Label;
use Anytimestream\UI\Row;
use Anytimestream\UI\Section;


require_once(__DIR__.'/../vendor/autoload.php');

$datasource = array();

$form = new Form("POST", "");
$form->setEndoing(Form::ENCODING_MULTIPART);
$headerSection = $form->addContainer(new Section());
$addressSection = $form->addContainer(new Section());
$row1 = $addressSection->addContainer(new Row());
$row1->addColumn(ColumnTypes::SM_6)->addView(new Label("Full Name", 'name', true));
$row1->addColumn(ColumnTypes::SM_6)->addView(new TextField('name', null, 'Your Full Name'));

$dateRow = $addressSection->addContainer(new Row());
$dateRow->addColumn(ColumnTypes::SM_6)->addView(new Label("Birth Date", 'bdate', true));
$dateRow->addColumn(ColumnTypes::SM_6)->addView(new DateField('bdate', "", "MM/DD/YYYY"));

$row2 = $addressSection->addContainer(new Row());
$row2->addColumn(ColumnTypes::SM_6)->addView(new Label("Address", null, true));
$column = $row2->addColumn(ColumnTypes::SM_6);
$innerRow1 = $column->addContainer(new Row());

$innerRow1->addColumn(ColumnTypes::SM_12)->addView(new TextField('line_1', null, 'Street Name/Number'));

$innerRow2 = $column->addContainer(new Row());
$innerRow2->addColumn(ColumnTypes::SM_12)->addView(new TextField('line_2', null, 'Drive'));

$innerRow3 = $column->addContainer(new Row());
$innerRow3->addAttribute('style', 'margin-bottom: 0!important');
$innerRow3->addColumn(ColumnTypes::SM_6)->addView(new TextField('state', null, 'State'));
$innerRow3->addColumn(ColumnTypes::SM_6)->addView(new TextField('city', null, 'City'));
$rowCountry = $addressSection->addContainer(new Row());
$rowCountry->addColumn(ColumnTypes::SM_6)->addView(new Label("Country", "country", true));
$rowCountry->addColumn(ColumnTypes::SM_6)->addView(new DropdownList('country', array('-' => '- Select -', 'ng' => 'Nigeria', 'us' => 'United States of America')));
$textAreaRow = $addressSection->addContainer(new Row());
$textAreaRow->addColumn(ColumnTypes::SM_6)->addView(new Label("Special Instructions", 'sp', true));
$textAreaRow->addColumn(ColumnTypes::SM_6)->addView(new TextArea('sp', "", 5));
$fileUploadRow = $addressSection->addContainer(new Row());
$fileUploadRow->addColumn(ColumnTypes::SM_6)->addView(new Label("Upload CV", 'cv', true));
$fileUploadRow->addColumn(ColumnTypes::SM_6)->addView(new FileUpload('cv', null, true));
$checkBoxRow = $addressSection->addContainer(new Row());
$checkBoxRow->addColumn(ColumnTypes::SM_6)->addView(new Label("Recieve Notifications", 'rn'));
$checkBoxRow->addColumn(ColumnTypes::SM_6)->addView(new Checkbox('rn'));
$rowTrigger = $addressSection->addContainer(new Row());
$rowTrigger->addColumn(ColumnTypes::SM_7);
$rowTrigger->addColumn(ColumnTypes::MD_2)->addView(new Button("Cancel", ButtonTypes::RESET));
$rowTrigger->addColumn(ColumnTypes::MD_2)->addView(new Button("Submit", ButtonTypes::SUBMIT));
?>
<html>
    <head>
        <title>Forms | UI</title>
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- Anytimestream UI -->
        <link href="../src/Anytimestream/Assets/css/ats-ui.css" rel="stylesheet">
    </head>
    <body>
        <br/><br/>
        <div class="container" style="width: 600px;">
            <?php
            if ($form->doPost()) {
                $name = $form->getValue('name');
                $bdate = $form->getValue('bdate');
                $line_1 = $form->getValue('line_1');
                $line_2 = $form->getValue('line_2');
                $state = $form->getValue('state');
                $city = $form->getValue('city');
                $country = $form->getValue('country');
                $cvs = $form->getFiles('cv');
                $specialInstructions = $form->getValue('sp');
                $recieveNotification = $form->getValue('rn');

                echo "Submitted, Name: $name<br/><br/>";

                if (strlen($name) < 3) {
                    $datasource['name']['error'] = "Invalid";
                }
                if (strlen($bdate) != 10) {
                    $datasource['bdate']['error'] = "Invalid";
                }
                if (strlen($line_1) < 3) {
                    $datasource['line_1']['error'] = "Invalid";
                }
                if (strlen($line_2) < 3) {
                    $datasource['line_2']['error'] = "Invalid";
                }
                if (strlen($state) < 3) {
                    $datasource['state']['error'] = "Invalid";
                }
                if (strlen($city) < 3) {
                    $datasource['city']['error'] = "Invalid";
                }
                if (strcasecmp($country, "-") == 0) {
                    $datasource['country']['error'] = "Invalid";
                }
                if (count(array_filter($cvs)) == 0) {
                    $datasource['cv']['error'] = "Invalid";
                }
                if ($recieveNotification == false) {
                    $datasource['rn']['error'] = "Invalid";
                }
                if (strlen($specialInstructions) < 3) {
                    $datasource['sp']['error'] = "Invalid";
                }
                $form->setDataSource($datasource);
            } else {
                $form->setDataSource($datasource);
            }
            $form->render();
            ?>
        </div>
        <!-- jQuery -->
        <script src="js/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>