<?php

function autoload($class) {
    $filename =  str_replace('\\', '/', dirname(BASE_PATH). '/src' . "/$class.php");
    if (is_readable($filename)) {
        require_once $filename;
    }
}

spl_autoload_register("autoload");



