<?php

use Anytimestream\UI\Columns;
use Anytimestream\UI\Form;
use Anytimestream\UI\Forms\Button;
use Anytimestream\UI\Forms\Buttons;
use Anytimestream\UI\Forms\Inputs\Checkbox;
use Anytimestream\UI\Forms\Inputs\DateField;
use Anytimestream\UI\Forms\Inputs\DropdownList;
use Anytimestream\UI\Forms\Inputs\FileUpload;
use Anytimestream\UI\Forms\Inputs\TextArea;
use Anytimestream\UI\Forms\Inputs\TextField;
use Anytimestream\UI\Forms\Label;
use Anytimestream\UI\RawContent;

//require_once 'config.php';
//require_once 'autoloader.php';

$datasource = array();

$form = new Form("POST", "");
$form->setEndoing(Form::ENCODING_MULTIPART);
$headerSection = $form->addSection("header");
$addressSection = $form->addSection("address");
$row1 = $addressSection->addRow();
$row1->addColumn(Columns::SM_6)->setContent(new Label("Full Name", 'name', true));
$row1->addColumn(Columns::SM_6)->setContent(new TextField('name', null, 'Your Full Name'));
$dateRow = $addressSection->addRow();
$dateRow->addColumn(Columns::SM_6)->setContent(new Label("Birth Date", 'bdate', true));
$dateRow->addColumn(Columns::SM_6)->setContent(new DateField('bdate', "", "MM/DD/YYYY"));
$row2 = $addressSection->addRow();
$row2->addColumn(Columns::SM_6)->setContent(new Label("Address", null, true));
$innerRow1 = $row2->addColumn(Columns::SM_6)->addRow();
$innerRow1->addColumn(Columns::SM_12)->setContent(new TextField('line_1', null, 'Street Name/Number'));
$innerRow2 = $innerRow1->getParent()->addColumn(Columns::LG_12)->addRow();
$innerRow2->addColumn(Columns::SM_12)->setContent(new TextField('line_2', null, 'Drive'));
$innerRow3 = $innerRow1->getParent()->addColumn(Columns::LG_12)->addRow();
$innerRow3->addAttribute('style', 'margin: 0!important');
$innerRow3->addColumn(Columns::SM_6)->setContent(new TextField('state', null, 'State'));
$innerRow3->addColumn(Columns::SM_1);
$innerRow3->addColumn(Columns::SM_5)->setContent(new TextField('city', null, 'City'));
$rowCountry = $addressSection->addRow();
$rowCountry->addColumn(Columns::SM_6)->setContent(new Label("Country", "country", true));
$rowCountry->addColumn(Columns::SM_6)->setContent(new DropdownList('country', array('-' => '- Select -', 'ng' => 'Nigeria', 'us' => 'United States of America')));
$textAreaRow = $addressSection->addRow();
$textAreaRow->addColumn(Columns::SM_6)->setContent(new Label("Special Instructions", 'sp', true));
$textAreaRow->addColumn(Columns::SM_6)->setContent(new TextArea('sp', "", 5));
$fileUploadRow = $addressSection->addRow();
$fileUploadRow->addColumn(Columns::SM_6)->setContent(new Label("Upload CV", 'cv', true));
$fileUploadRow->addColumn(Columns::SM_6)->setContent(new FileUpload('cv', null, true));
$checkBoxRow = $addressSection->addRow();
$checkBoxRow->addColumn(Columns::SM_6)->setContent(new Label("Recieve Notifications", 'rn'));
$checkBoxRow->addColumn(Columns::SM_6)->setContent(new Checkbox('rn'));
$rowTrigger = $addressSection->addRow();
$rowTrigger->addColumn(Columns::SM_7);
$rowTrigger->addColumn(Columns::MD_2)->setContent(new Button("Cancel", Buttons::RESET));
$rowTrigger->addColumn(Columns::MD_2)->setContent(new Button("Submut", Buttons::SUBMIT));
if($form->doPost()){
    $name = $form->getValue('name');
    $bdate = $form->getValue('bdate');
    $line_1 = $form->getValue('line_1');
    $line_2 = $form->getValue('line_2');
    $state = $form->getValue('state');
    $city = $form->getValue('city');
    $country = $form->getValue('country');
    $cvs = $form->getFiles('cv');
    $specialInstructions = $form->getValue('sp');
    $recieveNotification = $form->getValue('rn');
    
    //echo "Submitted, Name</br>";
    
    $headerSection->setContent(new RawContent("Submitted, Name: $name<br/><br/>"));
    
    if(strlen($name) < 3){
        $datasource['name']['error'] = "Invalid";
    }
    if(strlen($bdate) != 10){
        $datasource['bdate']['error'] = "Invalid";
    }
    if(strlen($line_1) < 3){
        $datasource['line_1']['error'] = "Invalid";
    }
    if(strlen($line_2) < 3){
        $datasource['line_2']['error'] = "Invalid";
    }
    if(strlen($state) < 3){
        $datasource['state']['error'] = "Invalid";
    }
    if(strlen($city) < 3){
        $datasource['city']['error'] = "Invalid";
    }
    if(strcasecmp($country, "-") == 0){
        $datasource['country']['error'] = "Invalid";
    }
    if(count(array_filter($cvs)) == 0){
        $datasource['cv']['error'] = "Invalid";
    }
    if($recieveNotification == false){
        $datasource['rn']['error'] = "Invalid";
    }
    if(strlen($specialInstructions) < 3){
        $datasource['sp']['error'] = "Invalid";
    }
    $form->setDataSource($datasource);
}
else {
    $form->setDataSource($datasource);
}
//$form->render();
