<?php

use Anytimestream\UI\Pagination;

require_once 'config.php';
require_once 'autoloader.php';

$currentPageIndex = filter_input(INPUT_GET, "page", FILTER_SANITIZE_NUMBER_INT);

$pagination = new Pagination();
$pagination->setCurrentIndex(isset($currentPageIndex)? $currentPageIndex: 1);
$pagination->setItemsPerPage(10);
$pagination->setTotalItemsCount(150);
$pagination->setNumberOfPagesToDisplay(9);
$pagination->setPageURL("http://dev.ui.anytimestream.com/tester/pagination.php");
?>
<html>
    <head>
        <title>Forms | UI</title>
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- Anytimestream UI -->
        <link href="../src/Anytimestream/Assets/css/ats-ui.css" rel="stylesheet">
    </head>
    <body>
        <br/><br/>
        <div class="container" style="width: 800px;">
            <?php
            $pagination->render();
            ?>
        </div>
        <!-- jQuery -->
        <script src="js/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>