<?php

use Anytimestream\UI\DataTables\Cells\NavigatorDataTableCell;
use Anytimestream\UI\DataTables\DataTable;

require_once(__DIR__.'/../vendor/autoload.php');

$links['menu'] = "Action";
$links['links'][] = Array('text' => 'Edit', 'url' => 'http://google.com');
$links['links'][] = Array('text' => 'Delete', 'url' => 'http://yahoo.com');

$datasource['headers'] = Array("Action", "Title", "URL", "Description", "CreationDate", "LastChanged");
$datasource['rows'][] = Array($links, "Introduction to Artificial Intelligence", "ai", "Robotics & AI", "27/04/2018 10:38:00", "27/04/2018 10:38:00");
$datasource['rows'][] = Array($links, "Amazon Web Services", "amzon", "Cloud Computing", "22/04/2018 7:18:20", "22/04/2018 7:18:20");
$datasource['rows'][] = Array($links, "Pragmatic Programmer", "book", "Programming Book", "02/04/2018 5:32:00", "02/04/2018 5:32:00");
$datasource['rows'][] = Array($links, "Refactoring", "book", "Programming Book", "23/04/2018 16:40:05", "23/04/2018 16:40:05");

$dataTable = new DataTable();
$dataTable->setDataTableCell(0, NavigatorDataTableCell::class);
$dataTable->setDatasource($datasource);

$header = $dataTable->getHeader(0);
//$header->addAttribute("style", "min-width: 300px;");
?>
<html>
    <head>
        <title>DataTable | UI</title>
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- Anytimestream UI -->
        <link href="../src/Anytimestream/Assets/css/ats-ui.css" rel="stylesheet">
    </head>
    <body>
        <br/><br/>
        <div class="container" style="width: 1000px;">
            <?php
            $dataTable->render();
            ?>
        </div>
        <!-- jQuery -->
        <script src="js/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>

