<?php
/* ~ CSSAsset.php
  .---------------------------------------------------------------------------.
  |  Software: Anytimestream Technologies Limited - UI                        |
  | ------------------------------------------------------------------------- |
  |     Admin: Norman Osaruyi (project admininistrator)                       |
  |   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
  |   Founder: Anytimestream Technologies Limited                             |
  | Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
  | ------------------------------------------------------------------------- |
  |   License: Distributed under the Lesser General Public License (LGPL)     |
  |            http://www.gnu.org/copyleft/lesser.html                        |
  | This program is distributed in the hope that it will be useful - WITHOUT  |
  | ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
  | FITNESS FOR A PARTICULAR PURPOSE.                                         |
  '---------------------------------------------------------------------------'
 */

namespace Anytimestream\UI;

/**
 * Anytimestream UI
 * class for HTML CSS Resource
 * @author Norman Osaruyi
 * @package Anytimestream\UI
 */
class CSSAsset extends Asset {

    private $href;

    /**
     * Creates new Instance
     * @param string $href url or asset
     */
    public function __construct(string $href) {
        parent::__construct();
        $this->href = $href;
        $this->initialize();
    }

    private function initialize() {
        $this->addAttribute("rel", "stylesheet");
        $this->addAttribute("href", $this->href);
    }

    public function render() {
        ?>
        <link <?= $this->getAttributesAsString() ?>>
        <?php
    }

}
