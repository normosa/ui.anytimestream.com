<?php

/* ~ Form.php
  .---------------------------------------------------------------------------.
  |  Software: Anytimestream Technologies Limited - UI                        |
  | ------------------------------------------------------------------------- |
  |     Admin: Norman Osaruyi (project admininistrator)                       |
  |   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
  |   Founder: Anytimestream Technologies Limited                             |
  | Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
  | ------------------------------------------------------------------------- |
  |   License: Distributed under the Lesser General Public License (LGPL)     |
  |            http://www.gnu.org/copyleft/lesser.html                        |
  | This program is distributed in the hope that it will be useful - WITHOUT  |
  | ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
  | FITNESS FOR A PARTICULAR PURPOSE.                                         |
  '---------------------------------------------------------------------------'
 */

namespace Anytimestream\UI;

use Anytimestream\UI\Inputs\Exception\InputNotFoundException;
use Anytimestream\UI\Inputs\Input;

/**
 * Anytimestream UI
 * class for HTML Form
 * @author Norman Osaruyi
 * @package Anytimestream\UI
 */
class Form extends GenericContainer {

    private $inputs;
    private $method;
    private $action;

    const ENCODING_MULTIPART = "multipart/form-data";

    /**
     * Creates new Instance
     * @param string $method Submit Method
     * @param string $action URL
     */
    public function __construct(string $method, string $action) {
        parent::__construct(ContainerTypes::FORM);
        $this->method = $method;
        $this->action = $action;
        $this->initialize();
    }

    /**
     * Initialize UI
     */
    private function initialize() {
        $this->addAttribute("method", $this->method);
        $this->addAttribute("action", $this->action);
        $this->addClass("ats-ui");
    }

    /**
     * Sets encoding
     * @param string $encoding enctype 
     */
    public function setEndoing(string $encoding) {
        $this->addAttribute("enctype", $encoding);
    }

    /**
     * Gets inputs count
     * @return int inputs count 
     */
    public function getInputCount(): int {
        return count($this->getInputs(true));
    }

    /**
     * Gets all Inputs
     * @param bool $reload optional clears any cache 
     * @return Array Inputs found
     */
    public function getInputs($reload = false): Array {
        if ($this->inputs == null || $reload == true){
            $this->searchForInput($this);
        }
        return $this->inputs;
    }

    /**
     * Gets Input
     * @param string $name name of input
     * @return Input Input or null if not found
     */
    public function getInput(string $name): Input {
        $this->ensureHasInput($name);
        return $this->inputs[$name];   
    }
    
    /**
     * Ensures Input is present
     * @param string $name name of input
     */
    private function ensureHasInput(string $name) {
        if (!$this->hasInput($name)) {
            throw new InputNotFoundException();
        }
    }
    
    /**
     * Checks for Input
     * @param string $name name of input
     * @return bool true or false
     */
    public function hasInput(string $name): bool {
        $this->getInputs();
        return isset($this->inputs[$name]);
    }

    /**
     * Gets Value from Input
     * @param string $name Name of Input to search
     */
    public function getValue(string $name) {
        $this->ensureHasInput($name);
        return $this->inputs[$name]->getValue();
    }

    /**
     * Gets Uploaded Files from Input
     * @param string $input Input to search
     * @return Array uploaded files
     */
    public function getFiles(string $input): Array {
        $files = $this->getValue($input);
        if ($files == null) {
            return null;
        }
        return array_filter($files, function($value) {
            return strlen($value[0]) > 1;
        });
    }

    /**
     * Sets DataSource for Inputs
     * @param array $datasource Associative Array containing values/errors for inputs indexed by Input name
     */
    public function setDataSource(Array $datasource) {
        $this->getInputs();
        foreach ($datasource as $name => $data) {
            if (isset($data['value'])) {
                $this->setValue($name, $data['value']);
            }
            if (isset($data['error'])) {
                $this->setError($name, $data['error']);
            }
        }
    }

    /**
     * Sets Value of Input
     * @param string $name Name of Input
     * @param string $value Value of Input
     */
    public function setValue(string $name, string $value) {
        $this->ensureHasInput($name);
        $this->inputs[$name]->setValue($value);
    }

    /**
     * Sets Error Message of Input
     * @param string $name Name of Input
     * @param string $message Error Message
     */
    public function setError(string $name, string $message) {
        $this->ensureHasInput($name);
        $this->inputs[$name]->setError($message);
    }

    /**
     * Recursive Search for Inputs
     * @param Container $container container to search
     */
    private function searchForInput(Container $container) {
        $childViews = $container->getViews();
        foreach ($childViews as $childView) {
            if ($childView instanceof Input) {
                $this->inputs[$childView->getName()] = $childView;
            } else if ($childView instanceof Container) {
                $this->searchForInput($childView);
            }
        }
    }

    /**
     * Retrieves Form Values
     * @return bool True if all values were present, or False is not 
     */
    public function doPost(): bool {
        $this->getInputs(true);
        foreach ($this->inputs as $input) {
            if (!$input->doPost()) {
                return false;
            }
        }
        return true;
    }

}
