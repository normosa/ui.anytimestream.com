<?php

/* ~ Attribute.php
  .---------------------------------------------------------------------------.
  |  Software: Anytimestream Technologies Limited - UI                        |
  | ------------------------------------------------------------------------- |
  |     Admin: Norman Osaruyi (project admininistrator)                       |
  |   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
  |   Founder: Anytimestream Technologies Limited                             |
  | Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
  | ------------------------------------------------------------------------- |
  |   License: Distributed under the Lesser General Public License (LGPL)     |
  |            http://www.gnu.org/copyleft/lesser.html                        |
  | This program is distributed in the hope that it will be useful - WITHOUT  |
  | ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
  | FITNESS FOR A PARTICULAR PURPOSE.                                         |
  '---------------------------------------------------------------------------'
 */

namespace Anytimestream\UI;

/**
 * Anytimestream UI
 * Interface for HTML Attributes
 * @author Norman Osaruyi
 * @package Anytimestream\UI
 */
interface Attribute {
    
    /**
     * Add attribute to UI
     * @param string $name Name of attribute
     * @param string $value Value of attribute
     */
    public function addAttribute(string $name, string $value);
    
    /**
     * Retrieves attribute from UI
     * @param string $name Name of attribute
     * @return string Value of attribute
     */
    public function getAttribute(string $name): string;
    
    /**
     * Checks if UI has attribute
     * @param string $name Name of attribute
     * @return bool true or false
     */
    public function hasAttribute(string $name): bool;
}
