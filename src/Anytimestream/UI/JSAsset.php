<?php
/* ~ JSAsset.php
  .---------------------------------------------------------------------------.
  |  Software: Anytimestream Technologies Limited - UI                        |
  | ------------------------------------------------------------------------- |
  |     Admin: Norman Osaruyi (project admininistrator)                       |
  |   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
  |   Founder: Anytimestream Technologies Limited                             |
  | Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
  | ------------------------------------------------------------------------- |
  |   License: Distributed under the Lesser General Public License (LGPL)     |
  |            http://www.gnu.org/copyleft/lesser.html                        |
  | This program is distributed in the hope that it will be useful - WITHOUT  |
  | ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
  | FITNESS FOR A PARTICULAR PURPOSE.                                         |
  '---------------------------------------------------------------------------'
 */

namespace Anytimestream\UI;

/**
 * Anytimestream UI
 * class for HTML JavaScript Resource
 * @author Norman Osaruyi
 * @package Anytimestream\UI
 */
class JSAsset extends Asset {

    private $src;
    private $view;

    /**
     * Creates new Instance
     * @param string $src link of resource
     * @param View $view contents
     */
    public function __construct(string $src, View $view = null) {
        parent::__construct();
        $this->src = $src;
        $this->view = $view;
        $this->initialize();
    }

    private function initialize() {
        $this->addAttribute("language", "javascript");
        $this->addAttribute("type", "text/javascript");
        if ($this->src != null) {
            $this->addAttribute("src", $this->src);
        }
    }

    /**
     * Displays UI
     */
    public function render() {
        ?>
        <script <?= $this->getAttributesAsString() ?>><?= (($this->view != null)) ? $this->view->render() : "" ?></script>
        <?php
    }

}
