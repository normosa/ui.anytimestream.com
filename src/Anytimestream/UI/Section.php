<?php

namespace Anytimestream\UI;

/* ~ Section.php
  .---------------------------------------------------------------------------.
  |  Software: Anytimestream Technologies Limited - UI                        |
  | ------------------------------------------------------------------------- |
  |     Admin: Norman Osaruyi (project admininistrator)                       |
  |   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
  |   Founder: Anytimestream Technologies Limited                             |
  | Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
  | ------------------------------------------------------------------------- |
  |   License: Distributed under the Lesser General Public License (LGPL)     |
  |            http://www.gnu.org/copyleft/lesser.html                        |
  | This program is distributed in the hope that it will be useful - WITHOUT  |
  | ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
  | FITNESS FOR A PARTICULAR PURPOSE.                                         |
  '---------------------------------------------------------------------------'
 */

/**
 * Anytimestream UI
 * derived class for Container
 * @author Norman Osaruyi
 * @package Anytimestream\UI
 */
class Section extends GenericContainer {
    
    /**
     * Creates new Instance
     * @param Container $parent optional Parent Container
     */
    public function __construct(Container $parent = null) {
        parent::__construct(ContainerTypes::SECTION, $parent);
    }
    
}
