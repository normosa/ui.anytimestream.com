<?php
/* ~ ListView.php
  .---------------------------------------------------------------------------.
  |  Software: Anytimestream Technologies Limited - UI                        |
  | ------------------------------------------------------------------------- |
  |     Admin: Norman Osaruyi (project admininistrator)                       |
  |   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
  |   Founder: Anytimestream Technologies Limited                             |
  | Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
  | ------------------------------------------------------------------------- |
  |   License: Distributed under the Lesser General Public License (LGPL)     |
  |            http://www.gnu.org/copyleft/lesser.html                        |
  | This program is distributed in the hope that it will be useful - WITHOUT  |
  | ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
  | FITNESS FOR A PARTICULAR PURPOSE.                                         |
  '---------------------------------------------------------------------------'
 */

namespace Anytimestream\UI;

use ArrayAccess;

/**
 * Anytimestream UI
 * class for ListView
 * @author Norman Osaruyi
 * @package Anytimestream\UI
 */
class ListView extends View implements ArrayAccess {
    
    public $views;
    
    /**
     * Creates new Instance
     * @param Array $views optional views Array
     */
    public function __construct(Array $views = Array()) {
        parent::__construct();
        $this->views = $views;
    }

    public function offsetExists($offset) {
        return isset($this->views[$offset]);
    }

    public function offsetGet($offset) {
        return $this->views[$offset];
    }

    public function offsetSet($offset, $value) {
        return $this->views[$offset] = $value;
    }

    public function offsetUnset($offset) {
        unset($this->views[$offset]);
    }

    /**
     * Gets no of Views
     * @return int views
     */
    public function count(): int {
        return count($this->views);
    }

    /**
     * Adds View
     * @param View $value view to add
     */
    public function add(View $value) {
        return $this->offsetSet($this->count(), $value);
    }
    
    /**
     * Displays UI
     */
    public function render() {
        foreach ($this->views as $view) {
            $view->render();
        }
    }
}

