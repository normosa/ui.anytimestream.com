<?php
/* ~ DropdownList.php
  .---------------------------------------------------------------------------.
  |  Software: Anytimestream Technologies Limited - UI                        |
  | ------------------------------------------------------------------------- |
  |     Admin: Norman Osaruyi (project admininistrator)                       |
  |   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
  |   Founder: Anytimestream Technologies Limited                             |
  | Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
  | ------------------------------------------------------------------------- |
  |   License: Distributed under the Lesser General Public License (LGPL)     |
  |            http://www.gnu.org/copyleft/lesser.html                        |
  | This program is distributed in the hope that it will be useful - WITHOUT  |
  | ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
  | FITNESS FOR A PARTICULAR PURPOSE.                                         |
  '---------------------------------------------------------------------------'
 */

namespace Anytimestream\UI\Inputs;

/**
 * Anytimestream UI
 * derived class for Form SelectInput
 * @author Norman Osaruyi
 * @package Anytimestream\UI\Inputs
 */
class DropdownList extends Input{
    
    private $options;
    
    
    /**
     * Creates new Instance
     * @param string $name name of input
     * @param array $options associative list of options
     * @param string $value optional selected option
     */
    public function __construct(string $name, array $options, string $value = null) {
        parent::__construct($name, $value);
        $this->options = $options;
    }
    
    
    /**
     * Displays input
     */
    public function render() {
        $this->startRender();
        $stringAttributes = $this->getAttributesAsString();
        echo "<select$stringAttributes>";
        foreach ($this->options as $key => $value) {
            if($this->value != null && strcmp($key, $this->value) == 0){
                echo '<option value="'.$key.'" selected>'.$value.'</option>';
            }
            else {
                echo '<option value="'.$key.'">'.$value.'</option>';
            }
        }
        echo "</select>";
        $this->endRender();
    }
}

