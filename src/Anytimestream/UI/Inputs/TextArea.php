<?php
/* ~ TextArea.php
  .---------------------------------------------------------------------------.
  |  Software: Anytimestream Technologies Limited - UI                        |
  | ------------------------------------------------------------------------- |
  |     Admin: Norman Osaruyi (project admininistrator)                       |
  |   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
  |   Founder: Anytimestream Technologies Limited                             |
  | Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
  | ------------------------------------------------------------------------- |
  |   License: Distributed under the Lesser General Public License (LGPL)     |
  |            http://www.gnu.org/copyleft/lesser.html                        |
  | This program is distributed in the hope that it will be useful - WITHOUT  |
  | ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
  | FITNESS FOR A PARTICULAR PURPOSE.                                         |
  '---------------------------------------------------------------------------'
 */

namespace Anytimestream\UI\Inputs;

/**
 * Anytimestream UI
 * derived class for Form TextArea
 * @author Norman Osaruyi
 * @package Anytimestream\UI\Inputs
 */
class TextArea extends Input{
    
    private $rows = "";
    
    /**
     * Creates new Instance
     * @param string $name name of input
     * @param string $value optional value of input
     * @param int $rows optional rows
     */
    public function __construct(string $name, string $value = null, int $rows = 1) {
        parent::__construct($name, $value);
        $this->rows = $rows;
        $this->initialize();
    }
    
    /**
     * Initialize UI
     */
    private function initialize() {
        $this->addAttribute("rows", $this->rows);
    }
    
    /**
     * Displays header
     */
    public function startRender() {
        parent::startRender();
        $this->addAttribute("name", $this->name);
    }
    
    /**
     * Displays input
     */
    public function render() {
        $this->startRender();
        $stringAttributes = $this->getAttributesAsString();
        echo "<textarea$stringAttributes>".$this->value."</textarea>";
        $this->endRender();
    }
}

