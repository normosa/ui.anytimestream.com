<?php

/* ~ FileUpload.php
  .---------------------------------------------------------------------------.
  |  Software: Anytimestream Technologies Limited - UI                        |
  | ------------------------------------------------------------------------- |
  |     Admin: Norman Osaruyi (project admininistrator)                       |
  |   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
  |   Founder: Anytimestream Technologies Limited                             |
  | Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
  | ------------------------------------------------------------------------- |
  |   License: Distributed under the Lesser General Public License (LGPL)     |
  |            http://www.gnu.org/copyleft/lesser.html                        |
  | This program is distributed in the hope that it will be useful - WITHOUT  |
  | ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
  | FITNESS FOR A PARTICULAR PURPOSE.                                         |
  '---------------------------------------------------------------------------'
 */

namespace Anytimestream\UI\Inputs;

/**
 * Anytimestream UI
 * derived class for Form FileUpload
 * @author Norman Osaruyi
 * @package Anytimestream\UI\Inputs
 */
class FileUpload extends Input{
    
    private $multiple;
    
    /**
     * Creates new Instance
     * @param string $name name of input
     * @param string $value optional value of input
     * @param bool $multiple optional allow multiple file
     */
    public function __construct(string $name, string $value = null, bool $multiple = false) {
        parent::__construct($name, $value);
        $this->multiple = $multiple;
        $this->initialize();
    }
    
    /**
     * Initialize UI
     */
    private function initialize() {
        $this->addAttribute("type", "file");
    }
    
    /**
     * Displays input
     */
    public function render() {
        $this->startRender();
        $stringAttributes = $this->getAttributesAsString();
        echo "<input$stringAttributes/>";
        $this->endRender();
    }
    
    /**
     * Displays header
     */
    public function startRender() {
        parent::startRender();
        if($this->multiple){
            $this->addAttribute("multiple", "multiple");
            $this->addAttribute("name", $this->name."[]");
        }
    }
    
    /**
     * Get Value of Input
     * @return mixed Value of Input
     */
    public function getValue() {
        return $_FILES[$this->name];
    }
    
    /**
     * Get Value from Post Data
     * @return bool True if Value was found else False
     */
    public function doPost(): bool {
        return isset($_FILES[$this->name]);
    }
}

