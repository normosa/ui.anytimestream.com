<?php
/* ~ Checkbox.php
  .---------------------------------------------------------------------------.
  |  Software: Anytimestream Technologies Limited - UI                        |
  | ------------------------------------------------------------------------- |
  |     Admin: Norman Osaruyi (project admininistrator)                       |
  |   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
  |   Founder: Anytimestream Technologies Limited                             |
  | Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
  | ------------------------------------------------------------------------- |
  |   License: Distributed under the Lesser General Public License (LGPL)     |
  |            http://www.gnu.org/copyleft/lesser.html                        |
  | This program is distributed in the hope that it will be useful - WITHOUT  |
  | ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
  | FITNESS FOR A PARTICULAR PURPOSE.                                         |
  '---------------------------------------------------------------------------'
 */

namespace Anytimestream\UI\Inputs;

/**
 * Anytimestream UI
 * derived class for Form Checkbox
 * @author Norman Osaruyi
 * @package Anytimestream\UI\Inputs
 */
class Checkbox extends Input{
    
    private $checked;
    
    /**
     * Creates new Instance
     * @param string $name name of input
     * @param bool $checked optional state of input
     */
    public function __construct(string $name, bool $checked = false) {
        parent::__construct($name, $checked);
        $this->checked = $checked;
        $this->initialize();
    }
    
    /**
     * Initialize UI
     */
    private function initialize() {
        $this->addAttribute("type", "checkbox");
    }
    
    /**
     * Displays input
     */
    public function render() {
        $this->startRender();
        $stringAttributes = $this->getAttributesAsString();
        echo "<input$stringAttributes/>";
        $this->endRender();
    }
    
    /**
     * Displays header
     */
    public function startRender() {
        parent::startRender();
        if($this->checked == true){
            $this->addAttribute("checked", "checked");
        }
        $this->addAttribute("value", $this->name);
    }
    
    /**
     * Get Value of Input
     * @return mixed Value of Input of false if not checked
     */
    public function getValue() {
        return $this->checked;
    }
    
    /**
     * Get Value from Post Data
     * @return bool True if Value was found else False
     */
    public function doPost(): bool {
        $this->checked = parent::doPost();
        $this->value = $this->name;
        return true;
    }
}

