<?php
/* ~ DateField.php
  .---------------------------------------------------------------------------.
  |  Software: Anytimestream Technologies Limited - UI                        |
  | ------------------------------------------------------------------------- |
  |     Admin: Norman Osaruyi (project admininistrator)                       |
  |   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
  |   Founder: Anytimestream Technologies Limited                             |
  | Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
  | ------------------------------------------------------------------------- |
  |   License: Distributed under the Lesser General Public License (LGPL)     |
  |            http://www.gnu.org/copyleft/lesser.html                        |
  | This program is distributed in the hope that it will be useful - WITHOUT  |
  | ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
  | FITNESS FOR A PARTICULAR PURPOSE.                                         |
  '---------------------------------------------------------------------------'
 */

namespace Anytimestream\UI\Inputs;

/**
 * Anytimestream UI
 * derived class for Form DateField
 * @author Norman Osaruyi
 * @package Anytimestream\UI\Inputs
 */
class DateField extends TextField{
    
    /**
     * Creates new Instance
     * @param string $name name of input
     * @param string $value optional value of input
     * @param string $placeholder optional placeholder text
     */
    public function __construct(string $name, string $value = null, string $placeholder = "") {
        parent::__construct($name, $value, $placeholder);
        $this->initialize();
    }
    
    /**
     * Initialize UI
     */
    private function initialize() {
        $this->addAttribute("type", "date");
    }
}

