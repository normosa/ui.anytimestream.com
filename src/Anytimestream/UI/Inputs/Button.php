<?php
/* ~ Button.php
  .---------------------------------------------------------------------------.
  |  Software: Anytimestream Technologies Limited - UI                        |
  | ------------------------------------------------------------------------- |
  |     Admin: Norman Osaruyi (project admininistrator)                       |
  |   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
  |   Founder: Anytimestream Technologies Limited                             |
  | Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
  | ------------------------------------------------------------------------- |
  |   License: Distributed under the Lesser General Public License (LGPL)     |
  |            http://www.gnu.org/copyleft/lesser.html                        |
  | This program is distributed in the hope that it will be useful - WITHOUT  |
  | ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
  | FITNESS FOR A PARTICULAR PURPOSE.                                         |
  '---------------------------------------------------------------------------'
 */

namespace Anytimestream\UI\Inputs;

use Anytimestream\UI\View;

/**
 * Anytimestream UI
 * derived class for Form Button
 * @author Norman Osaruyi
 * @package Anytimestream\UI\Inputs
 */
class Button extends View {

    private $action;
    private $buttonType;

    /**
     * Creates new Instance
     * @param string $action text of button
     * @param string $button_type submit or reset button type
     */
    public function __construct(string $action, string $button_type) {
        parent::__construct();
        $this->action = $action;
        $this->buttonType = $button_type;
        $this->initialize();
    }
    
    private function initialize(){
        $this->addAttribute("type", $this->buttonType);
    }

    /**
     * Displays button
     */
    public function render() {
        ?>
        <button<?= $this->getAttributesAsString() ?>><?= $this->action ?></button>
        <?php
    }

}
