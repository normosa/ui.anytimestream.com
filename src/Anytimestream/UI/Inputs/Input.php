<?php
/* ~ Input.php
  .---------------------------------------------------------------------------.
  |  Software: Anytimestream Technologies Limited - UI                        |
  | ------------------------------------------------------------------------- |
  |     Admin: Norman Osaruyi (project admininistrator)                       |
  |   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
  |   Founder: Anytimestream Technologies Limited                             |
  | Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
  | ------------------------------------------------------------------------- |
  |   License: Distributed under the Lesser General Public License (LGPL)     |
  |            http://www.gnu.org/copyleft/lesser.html                        |
  | This program is distributed in the hope that it will be useful - WITHOUT  |
  | ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
  | FITNESS FOR A PARTICULAR PURPOSE.                                         |
  '---------------------------------------------------------------------------'
 */

namespace Anytimestream\UI\Inputs;

use Anytimestream\UI\View;

/**
 * Anytimestream UI
 * abstract class for Form Input
 * @author Norman Osaruyi
 * @package Anytimestream\UI\Inputs
 */
abstract class Input extends View {

    protected $name;
    protected $value;
    protected $error = "";
    protected $filter = FILTER_SANITIZE_STRING;

    /**
     * Creates new Instance
     * @param string $name name of input
     * @param string $value optional value of input
     */
    public function __construct(string $name, string $value = null) {
        parent::__construct();
        $this->name = $name;
        $this->value = $value;
        $this->initialize();
    }
    
    private function initialize(){
        $this->addClass("input");
    }

    /**
     * Sets Name of Input
     * @param string $name Name to set
     */
    public function setName(string $name) {
        $this->name = $name;
    }

    /**
     * Gets Name of Input
     * @return string Name of Input
     */
    public function getName(): string {
        return $this->name;
    }

    /**
     * Sets Value of Input
     * @param string $value Value to set
     */
    public function setValue(string $value) {
        $this->value = $value;
    }

    /**
     * Gets Value of Input
     * @return mixed Value of Input
     */
    public function getValue() {
        return $this->value;
    }
    
    /**
     * Gets Error Message of Input
     * @return string error message
     */
    public function getError(): string{
        return $this->error;
    }

    /**
     * Sets Error message
     * @param string $error Message to set
     */
    public function setError(string $error) {
        $this->error = $error;
    }
    
    /**
     * Gets filter
     * @return string filter
     */
    public function getFilter(): string {
        return $this->filter;
    }
    
    /**
     * Sets filter
     * @return string filter
     */
    public function setFilter(string $filter){
        $this->filter = $filter;
    }

    /**
     * Get Value from Post Data
     * @return bool True if Value was found else False
     */
    public function doPost(): bool {
        $post_value = filter_input(INPUT_POST, $this->name, $this->filter);
        if (!isset($post_value)) {
            return false;
        }
        $this->value = $post_value;
        return true;
    }

    /**
     * Display header
     */
    public function startRender() {
        $this->addAttribute("id", $this->name);
        $this->addAttribute("name", $this->name);
        if(strlen($this->error) > 0){
            $this->addClass("error");
        }
    }

    /**
     * Display footer
     */
    public function endRender() {
        echo '<div class="error">' . $this->error . '</div>';
    }

}
