<?php
/* ~ Label.php
  .---------------------------------------------------------------------------.
  |  Software: Anytimestream Technologies Limited - UI                        |
  | ------------------------------------------------------------------------- |
  |     Admin: Norman Osaruyi (project admininistrator)                       |
  |   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
  |   Founder: Anytimestream Technologies Limited                             |
  | Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
  | ------------------------------------------------------------------------- |
  |   License: Distributed under the Lesser General Public License (LGPL)     |
  |            http://www.gnu.org/copyleft/lesser.html                        |
  | This program is distributed in the hope that it will be useful - WITHOUT  |
  | ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
  | FITNESS FOR A PARTICULAR PURPOSE.                                         |
  '---------------------------------------------------------------------------'
 */

namespace Anytimestream\UI;

/**
 * Anytimestream UI
 * class for Form Label
 * @author Norman Osaruyi
 * @package Anytimestream\UI
 */
class Label extends View {

    private $text;
    private $required;
    private $inputName;

    /**
     * Creates new Instance
     * @param string $text Text of Label
     * @param string $input_name Optional Label input
     * @param bool $required optional Require directive
     */
    public function __construct(string $text, string $input_name = null, bool $required = false) {
        parent::__construct();
        $this->text = $text;
        $this->required = $required;
        $this->inputName = $input_name;
        $this->initialize();
    }

    /**
     * Initialize UI
     */
    private function initialize() {
        $this->addClass("ats-ui");
        if ($this->inputName != null) {
            $this->addAttribute("for", $this->inputName);
        }
    }

    /**
     * Displays button
     */
    public function render() {
        ?>
        <label <?= $this->getAttributesAsString() ?>>
            <?= $this->text ?>
            <?php
            if($this->required){
                ?><span class="required">*</span><?php
            }
            ?>
        </label>
        <?php
    }

}
