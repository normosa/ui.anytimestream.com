<?php
/* ~ TextView.php
  .---------------------------------------------------------------------------.
  |  Software: Anytimestream Technologies Limited - UI                        |
  | ------------------------------------------------------------------------- |
  |     Admin: Norman Osaruyi (project admininistrator)                       |
  |   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
  |   Founder: Anytimestream Technologies Limited                             |
  | Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
  | ------------------------------------------------------------------------- |
  |   License: Distributed under the Lesser General Public License (LGPL)     |
  |            http://www.gnu.org/copyleft/lesser.html                        |
  | This program is distributed in the hope that it will be useful - WITHOUT  |
  | ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
  | FITNESS FOR A PARTICULAR PURPOSE.                                         |
  '---------------------------------------------------------------------------'
 */
namespace Anytimestream\UI;

/**
 * Anytimestream UI
 * class for Text content
 * @author Norman Osaruyi
 * @package Anytimestream\UI
 */
class TextView extends View{
    
    private $text = "";
    
    /**
     * Creates new Instance
     * @param string $text optional text
     */
    public function __construct(string $text = "") {
        parent::__construct();
        $this->text = $text;
    }
    
    /**
     * Sets Text
     * @param string $text Text to set
     */
    public function setText(string $text){
        $this->text = $text;
    }
    
    /**
     * Gets Text
     * @return string Text
     */
    public function getText(): string {
        return $this->text;
    }
    
    /**
     * Displays UI
     */
    public function render() {
        echo $this->text;
    }
}

