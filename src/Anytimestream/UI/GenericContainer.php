<?php

namespace Anytimestream\UI;

/* ~ GenericContainer.php
  .---------------------------------------------------------------------------.
  |  Software: Anytimestream Technologies Limited - UI                        |
  | ------------------------------------------------------------------------- |
  |     Admin: Norman Osaruyi (project admininistrator)                       |
  |   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
  |   Founder: Anytimestream Technologies Limited                             |
  | Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
  | ------------------------------------------------------------------------- |
  |   License: Distributed under the Lesser General Public License (LGPL)     |
  |            http://www.gnu.org/copyleft/lesser.html                        |
  | This program is distributed in the hope that it will be useful - WITHOUT  |
  | ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
  | FITNESS FOR A PARTICULAR PURPOSE.                                         |
  '---------------------------------------------------------------------------'
 */

/**
 * Anytimestream UI
 * derived class for Container
 * @author Norman Osaruyi
 * @package Anytimestream\UI
 */
class GenericContainer extends Container {

    private $type;

    /**
     * Creates new Instance
     * @param string $type ContainerType
     * @param Container $parent option Parent Container
     */
    public function __construct(string $type, Container $parent = null) {
        parent::__construct($parent);
        $this->views = Array();
        $this->type = $type;
    }
    
    /**
     * Gets type of container
     * @return string ContainerType
     */
    public function getType(): string {
        return $this->type;
    }
    
    /**
     * Sets type of Container
     * @param string $type ContainerType
     */
    public function setType(string $type) {
        $this->type = $type;
    }

    /**
     * Displays opening type
     */
    public function startRender() {
        ?><<?= $this->type .$this->getAttributesAsString() ?>><?php
    }

    /**
     * Displays end type
     */
    public function endRender() {
        ?></<?= $this->type ?>><?php
    }

}
