<?php

/* ~ DataTable.php
  .---------------------------------------------------------------------------.
  |  Software: Anytimestream Technologies Limited - UI                        |
  | ------------------------------------------------------------------------- |
  |     Admin: Norman Osaruyi (project admininistrator)                       |
  |   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
  |   Founder: Anytimestream Technologies Limited                             |
  | Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
  | ------------------------------------------------------------------------- |
  |   License: Distributed under the Lesser General Public License (LGPL)     |
  |            http://www.gnu.org/copyleft/lesser.html                        |
  | This program is distributed in the hope that it will be useful - WITHOUT  |
  | ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
  | FITNESS FOR A PARTICULAR PURPOSE.                                         |
  '---------------------------------------------------------------------------'
 */

namespace Anytimestream\UI;

use Anytimestream\UI\DataTableCells\DataTableCell;
use Anytimestream\UI\DataTableCells\TextDataTableCell;
use Anytimestream\UI\Exception\InvalidDatasourceException;
use Anytimestream\UI\Exception\ViewNotFoundException;


/**
 * Anytimestream UI
 * class for HTML Table
 * @author Norman Osaruyi
 * @package Anytimestream\UI
 */
class DataTable extends GenericContainer {

    private $datasource;
    private $dataTableCells;

    /**
     * Creates new Instance
     */
    public function __construct() {
        parent::__construct(ContainerTypes::TABLE);
        $this->initialize();
    }

    private function initialize() {
        $this->dataTableCells = Array();
        $this->addClass("table table-striped table-bordered dataTable no-footer");
        $this->addAttribute("role", "grid");
    }

    /**
     * Sets Datasource
     * @param Array $datasource Datasource of table
     */
    public function setDatasource(Array $datasource) {
        $this->datasource = $datasource;
        $this->ensureDatasourceIsValid();
        $this->datasourceChanged();
    }

    /**
     * Sets DataTableCell class of column
     * @param int $index index of column
     * @param string $cell classname of DataTableCell
     */
    public function setDataTableCell(int $index, string $cell) {
        if ($index > -1) {
            $this->dataTableCells[$index] = $cell;
        }
    }

    /**
     * Gets TextView of header
     * @param int $index index of column
     * @return TextView header
     */
    public function getHeader(int $index): TextView {
        $this->ensureDatasourceIsValid();
        $thead = $this->getViews();
        $tr = $thead[0]->getViews();
        $th = $tr[0]->getViews();
        if (isset($th[$index])) {
            return $th[$index]->getViews()[0];
        }
        throw new ViewNotFoundException();
    }

    private function setHeaders() {
        $thead = $this->addContainer(new GenericContainer(ContainerTypes::THEAD));
        $tr = $thead->addContainer(new GenericContainer(ContainerTypes::TR));
        $tr->addAttribute("role", "row");
        foreach ($this->datasource['headers'] as $header) {
            $th = $tr->addContainer(new GenericContainer(ContainerTypes::TH));
            $th->addView(new TextView($header));
        }
    }

    private function ensureDatasourceIsValid() {
        if (!$this->datasourceIsValid()) {
            throw new InvalidDatasourceException();
        }
    }

    /**
     * Checks if Datasource is valid
     * @return bool true or false
     */
    public function datasourceIsValid(): bool {
        return isset($this->datasource['headers']);
    }

    private function setBody() {
        if (!isset($this->datasource['rows'])) {
            return;
        }
        $tbody = $this->addContainer(new GenericContainer(ContainerTypes::TBODY));
        foreach ($this->datasource['rows'] as $row) {
            $tr = $tbody->addContainer(new GenericContainer(ContainerTypes::TR));
            $tr->addAttribute("role", "row");
            $index = 0;
            foreach ($row as $column) {
                $td = $tr->addContainer(new GenericContainer(ContainerTypes::TD));
                $dataTableCell = $this->createDataTableCell($index);
                $dataTableCell->setValue($column);
                $td->addView($dataTableCell);
                $index++;
            }
        }
    }

    /**
     * Creates new DataTableCell
     * @param int $index of column
     * @return DataTableCell created cell
     */
    private function createDataTableCell(int $index): DataTableCell {
        if (isset($this->dataTableCells[$index])) {
            return new $this->dataTableCells[$index];
        }
        return new TextDataTableCell();
    }

    private function datasourceChanged() {
        $this->clearAll();
        $this->setHeaders();
        $this->setBody();
    }

}
