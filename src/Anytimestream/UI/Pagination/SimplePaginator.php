<?php
/* ~ SimplePaginator.php
  .---------------------------------------------------------------------------.
  |  Software: Anytimestream Technologies Limited - UI                        |
  | ------------------------------------------------------------------------- |
  |     Admin: Norman Osaruyi (project admininistrator)                       |
  |   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
  |   Founder: Anytimestream Technologies Limited                             |
  | Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
  | ------------------------------------------------------------------------- |
  |   License: Distributed under the Lesser General Public License (LGPL)     |
  |            http://www.gnu.org/copyleft/lesser.html                        |
  | This program is distributed in the hope that it will be useful - WITHOUT  |
  | ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
  | FITNESS FOR A PARTICULAR PURPOSE.                                         |
  '---------------------------------------------------------------------------'
 */

namespace Anytimestream\UI\Pagination;

use Anytimestream\UI\ContainerTypes;
use Anytimestream\UI\GenericContainer;

/**
 * Anytimestream UI
 * class for HTML Pagination
 * @author Norman Osaruyi
 * @package Anytimestream\UI\Pagination
 */
class SimplePaginator extends GenericContainer implements Paginator {

    private $itemsPerPage;
    private $pageURL;
    private $totalItemsCount;
    private $currentIndex;
    private $numberOfPagesToDisiplay;

    /**
     * Creates new Instance
     */
    public function __construct() {
        parent::__construct(ContainerTypes::UL);
        $this->initialize();
    }

    private function initialize() {
        $this->addClass("pagination");
        $this->pageURL = "";
        $this->numberOfPagesToDisiplay = 10;
        $this->currentIndex = 1;
        $this->itemsPerPage = 10;
    }

    /**
     * Gets Items Per Page
     * @return int Items Per Page
     */
    public function getItemsPerPage(): int {
        return $this->itemsPerPage;
    }

    /**
     * Sets Items Per Page
     * @param int $itemsPerPage Items Per Page
     */
    public function setItemsPerPage(int $itemsPerPage) {
        $this->itemsPerPage = $itemsPerPage;
    }

    /**
     * Gets Next Page Index
     * @return int Page Index
     */
    public function getNextPageIndex(): int {
        return ($this->getNumberOfPages() > $this->currentIndex) ? $this->currentIndex + 1 : $this->currentIndex;
    }

    /**
     * Gets Previous Page Index
     * @return int Page Index
     */
    public function getPreviousPageIndex(): int {
        return ($this->currentIndex > 1) ? $this->currentIndex - 1 : $this->currentIndex;
    }

    /**
     * Gets Page URL
     * @return string Page URL
     */
    public function getPageURL(): string {
        return $this->pageURL;
    }

    /**
     * Sets Page URL
     * @param string $pageUrl Page URL
     */
    public function setPageURL(string $pageUrl) {
        $this->pageURL = $pageUrl;
    }

    /**
     * Gets Total Items Count
     * @return int Total Items Count
     */
    public function getTotalItemsCount(): int {
        return $this->totalItemsCount;
    }

    /**
     * Sets Total Items Count
     * @param int $totalItemsCount Total Items Count
     */
    public function setTotalItemsCount(int $totalItemsCount) {
        $this->totalItemsCount = $totalItemsCount;
    }

    /**
     * Gets Number of Pages to Display
     * @return int Number of Pages to Display
     */
    public function getNumberOfPagesToDisplay(): int {
        return $this->numberOfPagesToDisiplay;
    }

    /**
     * Sets Number of Pages to Display
     * @param int $numberOfPagesToDisplay Number of Pages to Display
     */
    public function setNumberOfPagesToDisplay(int $numberOfPagesToDisplay) {
        $this->numberOfPagesToDisiplay = $numberOfPagesToDisplay;
    }

    /**
     * Gets Current Index
     * @return int $currentIndex Index
     */
    public function getCurrentIndex(): int {
        return $this->currentIndex;
    }

    /**
     * Sets Current Index
     * @param int $currentIndex Index
     */
    public function setCurrentIndex(int $currentIndex) {
        $this->currentIndex = $currentIndex;
    }

    /**
     * Gets No of Pages
     * @return int pages
     */
    public function getNumberOfPages(): int {
        if ($this->totalItemsCount == 0) {
            return 0;
        }
        if ($this->totalItemsCount < $this->itemsPerPage) {
            return 1;
        }
        return (int) ceil($this->totalItemsCount / $this->itemsPerPage);
    }

    /**
     * Gets Pages
     * @return Array pages
     */
    private function getPages(): Array {
        return range(1, $this->getNumberOfPages());
    }

    /**
     * Gets index to start Display
     * @return int start index
     */
    private function getStartShowingIndex(): int {
        $indexToPad = (int) ceil($this->numberOfPagesToDisiplay / 2);
        $startingShowingIndex = $this->currentIndex - $indexToPad;
        $numberOfPages = $this->getNumberOfPages();
        if ($startingShowingIndex < 0) {
            $startingShowingIndex = 0;
        }
        if (($startingShowingIndex + $this->numberOfPagesToDisiplay) > $numberOfPages) {
            $startingShowingIndex = $numberOfPages - $this->numberOfPagesToDisiplay;
        }
        return $startingShowingIndex;
    }

    /**
     * Displays UI
     */
    public function render() {
        if ($this->getNumberOfPages() < 1) {
            return;
        }
        $this->startRender();
        $this->renderFixedButton("Previous", $this->getPreviousPageIndex());
        $this->renderButtons();
        $this->renderFixedButton("Next", $this->getNextPageIndex());
        $this->endRender();
    }

    /**
     * Fixed buttons
     * @param string $label label to show
     * @param int $pageIndex index of page
     */
    private function renderFixedButton(string $label, int $pageIndex) {
        $classStr = "";
        $hrefStr = ' href="' . $this->getPageURL() . '?' . $this->getQueryStringWithData("page", $pageIndex) . '"';
        if ($this->currentIndex == $pageIndex) {
            $classStr = ' class="disabled"';
            $hrefStr = '';
        }
        ?>
        <li<?= $classStr ?>>
            <a<?= $hrefStr ?>><?= $label ?></a>
        </li>
        <?php
    }

    /**
     * Render buttons
     */
    private function renderButtons() {
        $showPages = array_slice($this->getPages(), $this->getStartShowingIndex(), $this->numberOfPagesToDisiplay);
        for ($i = 0; $i < count($showPages); $i++):
            $hrefStr = '';
            if ($this->currentIndex != $showPages[$i]) {
                $hrefStr = ' href="' . $this->getPageURL() . '?' . $this->getQueryStringWithData("page", $showPages[$i]) . '"';
            }
            ?>
            <li class="<?= ($this->currentIndex == $showPages[$i]) ? "active" : "" ?>">
                <a<?= $hrefStr ?>><?= $showPages[$i] ?></a>
            </li>
            <?php
        endfor;
    }

    /**
     * Gets query string with new data
     * @param string $name name query
     * @param int $value value of query
     */
    private function getQueryStringWithData(string $name, string $value): string {
        $query_string = filter_input(INPUT_SERVER, 'QUERY_STRING', FILTER_SANITIZE_URL);
        $query = null;
        parse_str($query_string, $query);
        $query[$name] = $value;
        return http_build_query($query);
    }

}
