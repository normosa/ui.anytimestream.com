<?php

/* ~ Paginator.php
  .---------------------------------------------------------------------------.
  |  Software: Anytimestream Technologies Limited - UI                        |
  | ------------------------------------------------------------------------- |
  |     Admin: Norman Osaruyi (project admininistrator)                       |
  |   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
  |   Founder: Anytimestream Technologies Limited                             |
  | Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
  | ------------------------------------------------------------------------- |
  |   License: Distributed under the Lesser General Public License (LGPL)     |
  |            http://www.gnu.org/copyleft/lesser.html                        |
  | This program is distributed in the hope that it will be useful - WITHOUT  |
  | ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
  | FITNESS FOR A PARTICULAR PURPOSE.                                         |
  '---------------------------------------------------------------------------'
 */

namespace Anytimestream\UI\Pagination;

/**
 * Anytimestream UI
 * Interface for HTML Pagination
 * @author Norman Osaruyi
 * @package Anytimestream\UI\Pagination
 */
interface Paginator {
    
    /**
     * Gets Current Index
     * @return int $currentIndex Index
     */
    public function getCurrentIndex(): int;
    
    /**
     * Sets Current Index
     * @param int $currentIndex Index
     */
    public function setCurrentIndex(int $currentIndex);
    
    /**
     * Gets Page URL
     * @return string Page URL
     */
    public function getPageURL(): string;
    
    /**
     * Sets Page URL
     * @param string $pageUrl Page URL
     */
    public function setPageURL(string $pageUrl);
    
    
    /**
     * Gets Next Page Index
     * @return int Page Index
     */
    public function getNextPageIndex(): int;
    
    /**
     * Gets Previous Page Index
     * @return int Page Index
     */
    public function getPreviousPageIndex(): int;
    
    /**
     * Gets Number of Pages
     * @return int Number of Pages
     */
    public function getNumberOfPages(): int;
    
    /**
     * Gets Items Per Page
     * @return int Items Per Page
     */
    public function getItemsPerPage(): int;
    
    /**
     * Sets Items Per Page
     * @param int $itemsPerPage Items Per Page
     */
    public function setItemsPerPage(int $itemsPerPage);
    
    /**
     * Gets Total Items Count
     * @return int Total Items Count
     */
    public function getTotalItemsCount(): int;
    
    /**
     * Sets Total Items Count
     * @param int $totalItemsCount Total Items Count
     */
    public function setTotalItemsCount(int $totalItemsCount);
    
    /**
     * Gets Number of Pages to Display
     * @return int Number of Pages to Display
     */
    public function getNumberOfPagesToDisplay(): int;

    /**
     * Sets Number of Pages to Display
     * @param int $numberOfPagesToDisplay Number of Pages to Display
     */
    public function setNumberOfPagesToDisplay(int $numberOfPagesToDisplay);
    
}
