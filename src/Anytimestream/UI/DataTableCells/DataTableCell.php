<?php
/* ~ DataTableCell.php
  .---------------------------------------------------------------------------.
  |  Software: Anytimestream Technologies Limited - UI                        |
  | ------------------------------------------------------------------------- |
  |     Admin: Norman Osaruyi (project admininistrator)                       |
  |   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
  |   Founder: Anytimestream Technologies Limited                             |
  | Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
  | ------------------------------------------------------------------------- |
  |   License: Distributed under the Lesser General Public License (LGPL)     |
  |            http://www.gnu.org/copyleft/lesser.html                        |
  | This program is distributed in the hope that it will be useful - WITHOUT  |
  | ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
  | FITNESS FOR A PARTICULAR PURPOSE.                                         |
  '---------------------------------------------------------------------------'
 */

namespace Anytimestream\UI\DataTableCells;

use Anytimestream\UI\View;

/**
 * Anytimestream UI
 * class for HTML Table Cell
 * @author Norman Osaruyi
 * @package Anytimestream\UI\DataTableCells
 */
abstract class DataTableCell extends View {
    
    protected $value;
    
    /**
     * Creates new Instance
     */
    public function __construct() {
        parent::__construct();
    }
    
    /**
     * Sets Cell Value
     * @param mixed $value Value to set
     */
    public function setValue($value){
        $this->value = $value;
    }
    
    /**
     * Gets Cell Value
     * @return mixed Value of cell
     */
    public function getValue(){
        return $this->value;
    }
}

