<?php
/* ~ NavigatorDataTableCell.php
  .---------------------------------------------------------------------------.
  |  Software: Anytimestream Technologies Limited - UI                        |
  | ------------------------------------------------------------------------- |
  |     Admin: Norman Osaruyi (project admininistrator)                       |
  |   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
  |   Founder: Anytimestream Technologies Limited                             |
  | Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
  | ------------------------------------------------------------------------- |
  |   License: Distributed under the Lesser General Public License (LGPL)     |
  |            http://www.gnu.org/copyleft/lesser.html                        |
  | This program is distributed in the hope that it will be useful - WITHOUT  |
  | ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
  | FITNESS FOR A PARTICULAR PURPOSE.                                         |
  '---------------------------------------------------------------------------'
 */

namespace Anytimestream\UI\DataTableCells;

/**
 * Anytimestream UI
 * class for Raw HTML
 * @author Norman Osaruyi
 * @package Anytimestream\UI\DataTableCells
 */
class NavigatorDataTableCell extends DataTableCell {

    /**
     * Creates new Instance
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Displays UI
     */
    public function render() {
        ?>
        <div class="input-group-btn" style="position: absolute; z-index: 10">
            <button type="button" class="btn btn-default dropdown-toggle" style="padding: 2px 4px; font-size: 11px;" data-toggle="dropdown" aria-expanded="false">
                <?= $this->value['menu'] ?>
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu dropdown-menu-right" role="menu">
                <?php
                foreach ($this->value['links'] as $link) {
                    ?>
                    <li>
                        <a href="<?= $link['url'] ?>"><?= $link['text'] ?></a>
                    </li>
                    <?php
                }
                ?>
            </ul>
        </div>
        <?php
    }

}
