<?php
/* ~ ColumnTypes.php
  .---------------------------------------------------------------------------.
  |  Software: Anytimestream Technologies Limited - UI                        |
  | ------------------------------------------------------------------------- |
  |     Admin: Norman Osaruyi (project admininistrator)                       |
  |   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
  |   Founder: Anytimestream Technologies Limited                             |
  | Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
  | ------------------------------------------------------------------------- |
  |   License: Distributed under the Lesser General Public License (LGPL)     |
  |            http://www.gnu.org/copyleft/lesser.html                        |
  | This program is distributed in the hope that it will be useful - WITHOUT  |
  | ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
  | FITNESS FOR A PARTICULAR PURPOSE.                                         |
  '---------------------------------------------------------------------------'
 */

namespace Anytimestream\UI;

/**
 * Anytimestream UI
 * class for bootstrap Columns
 * @author Norman Osaruyi
 * @package Anytimestream\UI
 */
class ColumnTypes {
    const SM_1 = "col-sm-1";
    const SM_2 = "col-sm-2";
    const SM_3 = "col-sm-3";
    const SM_4 = "col-sm-4";
    const SM_5 = "col-sm-5";
    const SM_6 = "col-sm-6";
    const SM_7 = "col-sm-7";
    const SM_8 = "col-sm-8";
    const SM_9 = "col-sm-9";
    const SM_10 = "col-sm-10";
    const SM_11 = "col-sm-11";
    const SM_12 = "col-sm-12";
    const MD_1 = "col-md-1";
    const MD_2 = "col-md-2";
    const MD_3 = "col-md-3";
    const MD_4 = "col-md-4";
    const MD_5 = "col-md-5";
    const MD_6 = "col-md-6";
    const MD_7 = "col-md-7";
    const MD_8 = "col-md-8";
    const MD_9 = "col-md-9";
    const MD_10 = "col-md-10";
    const MD_11 = "col-md-11";
    const MD_12 = "col-md-12";
    const LG_1 = "col-lg-1";
    const LG_2 = "col-lg-2";
    const LG_3 = "col-lg-3";
    const LG_4 = "col-lg-4";
    const LG_5 = "col-lg-5";
    const LG_6 = "col-lg-6";
    const LG_7 = "col-lg-7";
    const LG_8 = "col-lg-8";
    const LG_9 = "col-lg-9";
    const LG_10 = "col-lg-10";
    const LG_11 = "col-lg-11";
    const LG_12 = "col-lg-12";
}

