<?php
/* ~ HTML.php
  .---------------------------------------------------------------------------.
  |  Software: Anytimestream Technologies Limited - UI                        |
  | ------------------------------------------------------------------------- |
  |     Admin: Norman Osaruyi (project admininistrator)                       |
  |   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
  |   Founder: Anytimestream Technologies Limited                             |
  | Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
  | ------------------------------------------------------------------------- |
  |   License: Distributed under the Lesser General Public License (LGPL)     |
  |            http://www.gnu.org/copyleft/lesser.html                        |
  | This program is distributed in the hope that it will be useful - WITHOUT  |
  | ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
  | FITNESS FOR A PARTICULAR PURPOSE.                                         |
  '---------------------------------------------------------------------------'
 */

namespace Anytimestream\UI;

/**
 * Anytimestream UI
 * abstract class for HTML tag
 * @author Norman Osaruyi
 * @package Anytimestream\UI
 */
class HTML extends GenericContainer {

    protected $title;
    
    protected $head;
    
    protected $body;

    /**
     * Creates new Instance
     * @param string $title optional Page title
     */
    public function __construct(string $title = "") {
        parent::__construct(ContainerTypes::HTML);
        $this->initialize($title);
    }
    
    /**
     * Initializes new Instance
     * @param string $title Page title
     */
    private function initialize(string $title){
        
        $this->head = $this->addContainer(new GenericContainer(ContainerTypes::HEAD));
        
        $this->body = $this->addContainer(new GenericContainer(ContainerTypes::BODY));
        
        $this->title = new TextView($title);
        
        $titleContainer = new GenericContainer(ContainerTypes::TITLE);
        $titleContainer->addView($this->title);
        
        $this->head->addContainer($titleContainer);
    }
    
    /**
     * Gets Body
     * @return Container body of HTML
     */
    public function getBody(): Container {
        return $this->body;
    }
    
    /**
     * Gets Head
     * @return Container head of HTML
     */
    public function getHead(): Container {
        return $this->head;
    }
    
    /**
     * Gets Title
     * @return string title of HTML
     */
    public function getTitle(): string {
        return $this->title->getText();
    }

    /**
     * Sets Title
     * @param string $title Title of HTML
     */
    public function setTitle(string $title) {
        $this->title->setText($title);
    }

    /**
     * Displays UI
     */
    public function render() {
        ?>
        <!DOCTYPE html>
        <?php
        parent::render();
    }

}
