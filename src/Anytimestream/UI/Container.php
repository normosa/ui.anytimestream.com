<?php

/* ~ Container.php
  .---------------------------------------------------------------------------.
  |  Software: Anytimestream Technologies Limited - UI                        |
  | ------------------------------------------------------------------------- |
  |     Admin: Norman Osaruyi (project admininistrator)                       |
  |   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
  |   Founder: Anytimestream Technologies Limited                             |
  | Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
  | ------------------------------------------------------------------------- |
  |   License: Distributed under the Lesser General Public License (LGPL)     |
  |            http://www.gnu.org/copyleft/lesser.html                        |
  | This program is distributed in the hope that it will be useful - WITHOUT  |
  | ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
  | FITNESS FOR A PARTICULAR PURPOSE.                                         |
  '---------------------------------------------------------------------------'
 */

namespace Anytimestream\UI;

/**
 * Anytimestream UI
 * abstract class for HTML Container Tags
 * @author Norman Osaruyi
 * @package Anytimestream\UI\Views
 */
abstract class Container extends View {
    
    private $views;
    
    /**
     * Creates new Instance
     * @param Container $parent option Parent Container
     */
    public function __construct(Container $parent = null) {
        parent::__construct($parent);
        $this->views = Array();
    }
    
    /**
     * Get child Views
     * @return array Views of Container
     */
    public function getViews(): Array {
        return $this->views;
    }
    
    /**
     * Get child views count
     * @return int no of child Views
     */
    public function getViewCount(): int {
        return count($this->views);
    }
    
    /**
     * Clear child views
     */
    public function clearAll() {
        $this->views = Array();
    }
    
    /**
     * Add child Container
     * @param Container $container container to add
     * @return Container container added
     */
    public function addContainer(Container $container): Container {
        return $this->addView($container);
    }
    
    /**
     * Add child Container
     * @param View $view view to add
     * @return View view added
     */
    public function addView(View $view): View {
        $view->setParent($this);
        $this->views[] = $view;
        return end($this->views);
    }
    
    /**
     * Displays UI
     */
    public function render(){
        $this->startRender();
        $this->renderViews();
        $this->endRender();
    }
    
    /**
     * Displays opening type
     */
    abstract function startRender();
    
    /**
     * Displays child views
     */
    private function renderViews() {
        foreach ($this->views as $view) {
            $view->render();
        }
    }
    
    /**
     * Displays end type
     */
    abstract function endRender();
}
