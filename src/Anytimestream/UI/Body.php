<?php
namespace Anytimestream\UI;

/* ~ Body.php
  .---------------------------------------------------------------------------.
  |  Software: Anytimestream Technologies Limited - UI                        |
  | ------------------------------------------------------------------------- |
  |     Admin: Norman Osaruyi (project admininistrator)                       |
  |   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
  |   Founder: Anytimestream Technologies Limited                             |
  | Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
  | ------------------------------------------------------------------------- |
  |   License: Distributed under the Lesser General Public License (LGPL)     |
  |            http://www.gnu.org/copyleft/lesser.html                        |
  | This program is distributed in the hope that it will be useful - WITHOUT  |
  | ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
  | FITNESS FOR A PARTICULAR PURPOSE.                                         |
  '---------------------------------------------------------------------------'
 */

/**
 * Anytimestream UI
 * derived class for HTML Body
 * @author Norman Osaruyi
 * @package Anytimestream\UI
 */
class Body extends GenericContainer {
    
    /**
     * Creates new Instance
     * @param Container $parent option Parent Container
     */
    public function __construct(Container $parent = null) {
        parent::__construct(ContainerTypes::BODY, $parent);
    }
    
}
