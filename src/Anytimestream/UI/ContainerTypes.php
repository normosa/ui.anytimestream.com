<?php
/* ~ ContainerTypes.php
  .---------------------------------------------------------------------------.
  |  Software: Anytimestream Technologies Limited - UI                        |
  | ------------------------------------------------------------------------- |
  |     Admin: Norman Osaruyi (project admininistrator)                       |
  |   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
  |   Founder: Anytimestream Technologies Limited                             |
  | Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
  | ------------------------------------------------------------------------- |
  |   License: Distributed under the Lesser General Public License (LGPL)     |
  |            http://www.gnu.org/copyleft/lesser.html                        |
  | This program is distributed in the hope that it will be useful - WITHOUT  |
  | ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
  | FITNESS FOR A PARTICULAR PURPOSE.                                         |
  '---------------------------------------------------------------------------'
 */

namespace Anytimestream\UI;

/**
 * Anytimestream UI
 * class for HTML Container Tags Placeholder
 * @author Norman Osaruyi
 * @package Anytimestream\UI
 */
class ContainerTypes {
    const TITLE = "title";
    const DIV = "div";
    const FORM = "form";
    const SECTION = "section";
    const COLUMN = "div";
    const HEADER = "header";
    const HTML = "html";
    const HEAD = "head";
    const BODY = "body";
    const FOOTER = "footer";
    const TABLE = "Table";
    const THEAD = "thead";
    const TBODY = "tbody";
    const TR = "tr";
    const TH = "th";
    const TD = "td";
    const UL = "ul";
}

