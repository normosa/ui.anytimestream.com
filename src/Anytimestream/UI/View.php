<?php
/* ~ View.php
  .---------------------------------------------------------------------------.
  |  Software: Anytimestream Technologies Limited - UI                        |
  | ------------------------------------------------------------------------- |
  |     Admin: Norman Osaruyi (project admininistrator)                       |
  |   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
  |   Founder: Anytimestream Technologies Limited                             |
  | Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
  | ------------------------------------------------------------------------- |
  |   License: Distributed under the Lesser General Public License (LGPL)     |
  |            http://www.gnu.org/copyleft/lesser.html                        |
  | This program is distributed in the hope that it will be useful - WITHOUT  |
  | ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
  | FITNESS FOR A PARTICULAR PURPOSE.                                         |
  '---------------------------------------------------------------------------'
 */

namespace Anytimestream\UI;

use Anytimestream\UI\Exception\AttributeNotFoundException;

/**
 * Anytimestream UI
 * abstract class for all UI
 * @author Norman Osaruyi
 * @package Anytimestream\UI
 */
abstract class View implements Attribute {
    
    private $attributes;
    
    private $parent;

    /**
     * Creates new Instance
     * @param Container $parent optional parent
     */
    public function __construct(Container $parent = null) {
        $this->attributes = array();
        $this->parent = $parent;
    }
    
    /**
     * Adds attribute to UI
     * @param string $name Name of attribute
     * @param string $value Value of attribute
     */
    public function addAttribute(string $name, string $value){
        $this->attributes[$name] = $value;
    }
    
    /**
     * Retrieves attribute from UI
     * @param string $name Name of attribute
     * @return string Value of attribute
     */
    public function getAttribute(string $name): string {
        if(!$this->hasAttribute($name)){
            throw new AttributeNotFoundException;
        }
        return $this->attributes[$name];
    }
    
    /**
     * Checks if has attribute
     * @param string $name Name of attribute
     * @return bool True or False
     */
    public function hasAttribute(string $name): bool{
        return isset($this->attributes[$name]);
    }
    
    /**
     * Removes attribute
     * @param string $name Name of attribute
     */
    public function removeAttribute($name){
        if($this->hasAttribute($name)){
            unset($this->attributes[$name]);
        }
    }
    
    /**
     * Adds css class
     * @param string $class Name of css class
     */
    public function addClass(string $class){
        if($this->hasAttribute("class")){
            $this->addAttribute("class", $this->getAttribute("class")." $class");
        }
        else {
            $this->addAttribute("class", $class);
        }
    }
    
    /**
     * Builds attributes as string
     * @return string name/value pair of attributes
     */
    protected function getAttributesAsString(): string{
        $attributes = array_map(function($value, $key){
            return $key.'="'.$value.'"';
        }, array_values($this->attributes), array_keys($this->attributes));
        $attributeString = implode(" ", $attributes);
        return (count($this->attributes) > 0)? " ".$attributeString: "";
    }
    
    /**
     * Sets Parent Container
     * @param Container $parent Parent Container
     */
    public function setParent(Container $parent) {
        $this->parent = $parent;
    }
    
    /**
     * Gets Parent Container
     * @return Container Parent Container
     */
    public function getParent(): Container {
        return $this->parent;
    }
    
    /**
     * Displays UI
     */
    abstract public function render();
}

