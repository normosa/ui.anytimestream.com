<?php
/* ~ SimpleHTML.php
  .---------------------------------------------------------------------------.
  |  Software: Anytimestream Technologies Limited - UI                        |
  | ------------------------------------------------------------------------- |
  |     Admin: Norman Osaruyi (project admininistrator)                       |
  |   Authors: Norman Osaruyi norman.osaruyi@anytimestream.com                |
  |   Founder: Anytimestream Technologies Limited                             |
  | Copyright (c) 2018. Anytimestream Technologies LTD. All Rights Reserved.  |
  | ------------------------------------------------------------------------- |
  |   License: Distributed under the Lesser General Public License (LGPL)     |
  |            http://www.gnu.org/copyleft/lesser.html                        |
  | This program is distributed in the hope that it will be useful - WITHOUT  |
  | ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or     |
  | FITNESS FOR A PARTICULAR PURPOSE.                                         |
  '---------------------------------------------------------------------------'
 */

namespace Anytimestream\UI;

/**
 * Anytimestream UI
 * class for HTML Page
 * @author Norman Osaruyi
 * @package Anytimestream\UI
 */
class SimpleHTML extends HTML {
    
    const LAYOUTS_CONTAINER_FLUID = "container-fluid";
    const LAYOUTS_CONTAINER = "container";
    
    protected $header;
    
    protected $section;
    
    protected $footer;
    
    private $layout;


    /**
     * Creates new Instance
     * @param string $title optional HTML title
     * @param string $layout optional bootstrap layout
     */
    public function __construct(string $title = "", string $layout = self::LAYOUTS_CONTAINER_FLUID) {
        parent::__construct($title);
        $this->layout = $layout;
        $this->initialize();
    }
    
    private function initialize(){
        $this->header = new GenericContainer(ContainerTypes::HEADER);
        $this->header->addClass($this->layout);
        
        $this->section = new Section();
        $this->section->addClass($this->layout);
        
        $this->footer = new GenericContainer(ContainerTypes::FOOTER);
        $this->footer->addClass($this->layout);
        
        $this->body->addContainer($this->header);
        $this->body->addContainer($this->section);
        $this->body->addContainer($this->footer);
    }
    
    /**
     * Gets Header
     * @return Container header of HTML
     */
    public function getHeader(): Container {
        return $this->header;
    }
    
    /**
     * Gets Footer
     * @return Container footer of HTML
     */
    public function getFooter(): Container {
        return $this->footer;
    }
    
    /**
     * Gets main Section
     * @return Container main section of HTML
     */
    public function getSection(): Container {
        return $this->section;
    }
}