<?php
namespace tests;

use Anytimestream\UI\ColumnTypes;
use Anytimestream\UI\ContainerTypes;
use Anytimestream\UI\Row;
use PHPUnit\Framework\TestCase;

require_once(__DIR__.'/../vendor/autoload.php');

class RowTest extends TestCase {
    
    public function testRender() {
        $row = new Row();
        
        $row->addColumn(ColumnTypes::LG_1);
        
        $htmlString = Util::GetContentRenderAsString($row);
        
        $this->assertEquals(true, strpos($htmlString, '<'.ContainerTypes::DIV.' class="row">') !== false);
        
        $this->assertEquals(true, strpos($htmlString, 'class="'.ColumnTypes::LG_1.'">') !== false);
        
    }
    
}
    