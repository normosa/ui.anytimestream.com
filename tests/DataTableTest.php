<?php

namespace tests;

use Anytimestream\UI\ContainerTypes;
use Anytimestream\UI\DataTable;
use Anytimestream\UI\DataTableCells\NavigatorDataTableCell;
use Anytimestream\UI\Exception\InvalidDatasourceException;
use Anytimestream\UI\Exception\ViewNotFoundException;
use PHPUnit\Framework\TestCase;

require_once(__DIR__ . '/../vendor/autoload.php');

class DataTableTest extends TestCase {

    public function testRender() {
        $dataTable = $this->createDataTable();
        $dataTable->setDataTableCell(0, NavigatorDataTableCell::class);

        $datasource = $this->createDatasourceWithCustomCell();
        
        $dataTable->setDatasource($datasource);

        $htmlString = Util::GetContentRenderAsString($dataTable);

        $this->assertEquals(true, strpos($htmlString, '<' . ContainerTypes::TABLE) !== false);
        $this->assertEquals(true, strpos($htmlString, $datasource['rows'][0][0]['menu']) !== false);
        $this->assertEquals(true, strpos($htmlString, $datasource['rows'][0][0]['links'][0]['text']) !== false);
        $this->assertEquals(true, strpos($htmlString, $datasource['rows'][0][0]['links'][0]['url']) !== false);
        $this->assertEquals(true, strpos($htmlString, $datasource['rows'][1][1]) !== false);
        $this->assertEquals(true, strpos($htmlString, $datasource['rows'][1][2]) !== false);
        $this->assertEquals(true, strpos($htmlString, $datasource['headers'][0]) !== false);
        $this->assertEquals(true, strpos($htmlString, $datasource['headers'][2]) !== false);
        $this->assertEquals(true, strpos($htmlString, $datasource['headers'][5]) !== false);
    }
    
    public function testHeader() {
        $dataTable = $this->createDataTable();

        $datasource = $this->createDatasource();
        
        $dataTable->setDatasource($datasource);
        
        $textView = $dataTable->getHeader(0);
        
        try{
            $dataTable->getHeader(6);
        } catch (ViewNotFoundException $ex) {
            $this->assertEquals(true, true);
        }

        $htmlString = Util::GetContentRenderAsString($textView);

        $this->assertEquals(true, strpos($htmlString, $datasource['headers'][0]) !== false);
    }
    
    public function testDatasource() {
        $dataTable = $this->createDataTable();
        
        try{
            $dataTable->getHeader(6);
        } catch (InvalidDatasourceException $ex) {
            $this->assertEquals(true, true);
        }
        
        $this->assertEquals(false, $dataTable->datasourceIsValid());
        
        $dataTable->setDatasource($this->createDatasource());
        
        $this->assertEquals(true, $dataTable->datasourceIsValid());
    }

    private function createDataTable() {
        $dataTable = new DataTable();
        $dataTable->setDataTableCell(0, "Anytimestream\UI\DataTables\Cells\NavigatorDataTableCell");

        return $dataTable;
    }
    
    private function createDatasource() {
        $datasource['headers'] = Array("Action", "Title", "URL", "Description", "CreationDate", "LastChanged");
        
        return $datasource;
    }

    private function createDatasourceWithCustomCell() {
        $links['menu'] = "Action";
        $links['links'][] = Array('text' => 'Edit', 'url' => 'http://google.com');
        $links['links'][] = Array('text' => 'Delete', 'url' => 'http://yahoo.com');

        $datasource['headers'] = Array("Action", "Title", "URL", "Description", "CreationDate", "LastChanged");
        $datasource['rows'][] = Array($links, "Introduction to Artificial Intelligence", "ai", "Robotics & AI", "27/04/2018 10:38:00", "27/04/2018 10:38:00");
        $datasource['rows'][] = Array($links, "Amazon Web Services", "amzon", "Cloud Computing", "22/04/2018 7:18:20", "22/04/2018 7:18:20");
        $datasource['rows'][] = Array($links, "Pragmatic Programmer", "book", "Programming Book", "02/04/2018 5:32:00", "02/04/2018 5:32:00");
        $datasource['rows'][] = Array($links, "Refactoring", "book", "Programming Book", "23/04/2018 16:40:05", "23/04/2018 16:40:05");
        
        return $datasource;
    }

}
