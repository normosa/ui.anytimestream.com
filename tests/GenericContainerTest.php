<?php
namespace tests;

use Anytimestream\UI\ContainerTypes;
use Anytimestream\UI\GenericContainer;
use Anytimestream\UI\Row;
use PHPUnit\Framework\TestCase;

require_once(__DIR__.'/../vendor/autoload.php');

class GenericContainerTest extends TestCase {

    public function testRender() {
        $container = new GenericContainer(ContainerTypes::DIV);
        
        $this->assertEquals(true, $container->getViewCount() == 0);
        
        $row1 = $container->addContainer(new Row());
        
        $id = "address";
        
        $row1->addAttribute("id", $id);
        
        $this->assertEquals(true, $container->getViewCount() == 1);
        
        $container->addContainer(new Row())->addContainer(new Row());
        
        $this->assertEquals(true, $container->getViewCount() == 2);
        
        $htmlString = Util::GetContentRenderAsString($container);

        $this->assertEquals(true, strpos($htmlString, '<'.ContainerTypes::DIV) !== false);
        $this->assertEquals(true, strpos($htmlString, '</'.ContainerTypes::DIV.'>') !== false);
        $this->assertEquals(true, strpos($htmlString, '<'.ContainerTypes::DIV.' class="row">') !== false);
        $this->assertEquals(true, strpos($htmlString, ' id="'.$id.'"') !== false);
    }
}
