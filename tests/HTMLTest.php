<?php
namespace tests;

use Anytimestream\UI\ContainerTypes;
use Anytimestream\UI\HTML;
use PHPUnit\Framework\TestCase;

require_once(__DIR__.'/../vendor/autoload.php');

class HTMLTest extends TestCase {
    
    public function testRender() {
        $html = new HTML();
        
        $htmlString = Util::GetContentRenderAsString($html);
        
        $this->assertEquals(true, strpos($htmlString, '<'.ContainerTypes::HTML.'>') !== false);
        $this->assertEquals(true, strpos($htmlString, '</'.ContainerTypes::HTML.'>') !== false);
        $this->assertEquals(true, strpos($htmlString, '<'.ContainerTypes::HEAD.'>') !== false);
        $this->assertEquals(true, strpos($htmlString, '</'.ContainerTypes::HEAD.'>') !== false);
        $this->assertEquals(true, strpos($htmlString, '<'.ContainerTypes::TITLE.'>') !== false);
        $this->assertEquals(true, strpos($htmlString, '</'.ContainerTypes::TITLE.'>') !== false);
        $this->assertEquals(true, strpos($htmlString, '<'.ContainerTypes::BODY.'>') !== false);
        $this->assertEquals(true, strpos($htmlString, '</'.ContainerTypes::BODY.'>') !== false);
        
    }
    
    public function testTitle() {
        $title = "HTML Page";
        $html = new HTML($title);
        
        $this->assertEquals(true, strcmp($html->getTitle(), $title) == 0);
        
        $htmlString = Util::GetContentRenderAsString($html);
        
        $this->assertEquals(true, strpos($htmlString, $title) !== false);
        
        $title2 = "Hello Wrld!";
        $html->setTitle($title2);
        
        $htmlString2 = Util::GetContentRenderAsString($html);
        
        $this->assertEquals(true, strpos($htmlString2, $title2) !== false);
        
    }
    
    public function testBody() {
        $html = new HTML();
        $body = $html->getBody();
        
        $htmlString = Util::GetContentRenderAsString($body);
        
        $this->assertEquals(true, strpos($htmlString, '<'.ContainerTypes::BODY) !== false);   
    }
    
    public function testHead() {
        $html = new HTML();
        $head = $html->getHead();
        
        $htmlString = Util::GetContentRenderAsString($head);
        
        $this->assertEquals(true, strpos($htmlString, '<'.ContainerTypes::HEAD) !== false);   
    }
    
}
    