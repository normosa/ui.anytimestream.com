<?php
namespace tests\Pagination;

use Anytimestream\UI\Pagination\SimplePaginator;
use PHPUnit\Framework\TestCase;
use tests\Util;

require_once(__DIR__.'/../../vendor/autoload.php');

class SimplePaginatorTest extends TestCase {

    const URL = "http://dev.ui.anytimestream.com/tester/pagination.php";

    public function testProperties() {
        $paginator = new SimplePaginator();
        $paginator->setCurrentIndex(1);
        $paginator->setItemsPerPage(10);
        $paginator->setTotalItemsCount(151);
        $paginator->setNumberOfPagesToDisplay(9);
        $paginator->setPageURL(self::URL);
        $this->assertEquals(true, $paginator->getPreviousPageIndex() == 1);
        $this->assertEquals(true, $paginator->getNextPageIndex() == 2);
        $this->assertEquals(true, $paginator->getNumberOfPages() == 16);
        
        $paginator->setItemsPerPage(7);
        $paginator->setTotalItemsCount(45);
        $this->assertEquals(true, $paginator->getNumberOfPages() == 7);
        
        $paginator->setItemsPerPage(10);
        $paginator->setTotalItemsCount(150);

        return $paginator;
    }

    /**
     * @depends testProperties
     */
    public function testCurrentPageIndex(SimplePaginator $paginator) {
        $htmlString = Util::GetContentRenderAsString($paginator);

        $this->assertEquals(true, strpos($htmlString, '<li class="active">') !== false);
        
        $paginator->setCurrentIndex(5);
        $htmlString2 = Util::GetContentRenderAsString($paginator);
        $this->assertEquals(true, strpos($htmlString2, '<a>5</a>') !== false);
    }

    /**
     * @depends testProperties
     */
    public function testNumberOfPagesToDisplay(SimplePaginator $paginator) {
        $htmlString = Util::GetContentRenderAsString($paginator);

        $this->assertEquals(true, strpos($htmlString, '<a href="' . self::URL . '?page=9">9</a>') !== false);
        $this->assertEquals(true, strpos($htmlString, '<a href="' . self::URL . '?page=1">1</a>') !== false);
        $this->assertEquals(false, strpos($htmlString, '<a href="' . self::URL . '?page=10">10</a>') !== false);

        $paginator->setCurrentIndex(5);
        $htmlString2 = Util::GetContentRenderAsString($paginator);

        $this->assertEquals(false, strpos($htmlString2, '<a href="' . self::URL . '?page=10">10</a>') !== false);

        $paginator->setCurrentIndex(6);
        $htmlString3 = Util::GetContentRenderAsString($paginator);

        $this->assertEquals(false, strpos($htmlString3, '<a href="' . self::URL . '?page=1">1</a>') !== false);
        $this->assertEquals(false, strpos($htmlString3, '<a href="' . self::URL . '?page=6">6</a>') !== false);
        $this->assertEquals(true, strpos($htmlString3, '<a href="' . self::URL . '?page=10">10</a>') !== false);
    }
    
    /**
     * @depends testProperties
     */
    public function testFixedButtons(SimplePaginator $paginator) {
        $paginator->setCurrentIndex(5);
        
        $htmlString = Util::GetContentRenderAsString($paginator);

        $this->assertEquals(true, strpos($htmlString, '<a href="' . self::URL . '?page=4">Previous</a>') !== false);
        $this->assertEquals(true, strpos($htmlString, '<a href="' . self::URL . '?page=6">Next</a>') !== false);
    }

    /**
     * @depends testProperties
     */
    public function testPageURL(SimplePaginator $paginator) {
        $paginator->setPageURL(self::URL);
        $htmlString = Util::GetContentRenderAsString($paginator);

        $this->assertEquals(true, strpos($htmlString, '<a href="' . self::URL . '?page=2">2</a>') !== false);
    }

    /**
     * @depends testProperties
     */
    public function testWithEmptyItems(SimplePaginator $paginator) {
        $paginator->setTotalItemsCount(0);
                
        $htmlString = Util::GetContentRenderAsString($paginator);
        $this->assertEquals(true, strlen($htmlString) == 0);
    }
}
