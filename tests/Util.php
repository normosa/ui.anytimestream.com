<?php
namespace tests;

use Anytimestream\UI\View;

class Util {
    
    public static function GetContentRenderAsString(View $view): string {
        ob_start();
        $view->render();
        return ob_get_clean();
    }
}

