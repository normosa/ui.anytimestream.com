<?php
namespace tests\Inputs;

use Anytimestream\UI\Inputs\Checkbox;
use PHPUnit\Framework\TestCase;
use tests\Util;

require_once(__DIR__.'/../../vendor/autoload.php');

class CheckboxTest extends TestCase {

    const NAME = "recieveNotification";
    const ERROR = "Invalid";

    public function testProperties() {

        $checkBox = self::CreateCheckBox();
        
        $this->assertEquals(true, self::NAME === $checkBox->getName());

        $this->assertEquals(true, $checkBox->doPost());

        $this->assertEquals(false, $checkBox->getValue());
    }

    public function testRender() {
        $checkBox = self::CreateCheckBox();
        $htmlString = Util::GetContentRenderAsString($checkBox);
        $this->assertEquals(true, strpos($htmlString, '<input ') !== false);
        $this->assertEquals(true, strpos($htmlString, 'name="' . self::NAME . '"') !== false);
        $this->assertEquals(true, strpos($htmlString, 'type="checkbox"') !== false);
        $this->assertEquals(false, strpos($htmlString, '<div class="error">'.self::ERROR.'</div>') !== false);
        
        $checkBox->setError(self::ERROR);
        $htmlString2 = Util::GetContentRenderAsString($checkBox);
        
        $this->assertEquals(true, strpos($htmlString2, '<div class="error">'.self::ERROR.'</div>') !== false);
    }
    
    private static function CreateCheckBox(){
        return new Checkbox(self::NAME);
    }

}
