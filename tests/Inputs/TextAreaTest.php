<?php
namespace tests\Inputs;

use tests\Util;
use Anytimestream\UI\Inputs\TextArea;
use PHPUnit\Framework\TestCase;

require_once(__DIR__.'/../../vendor/autoload.php');

class TextAreaTest extends TestCase {

    const NAME = "message";
    const ROWS = 5;
    const VALUE = "Hello from Kenwood";
    const ERROR = "Invalid";

    public function testProperties() {

        $textArea = self::CreateTextArea();
        
        $this->assertEquals(true, self::NAME === $textArea->getName());

        $this->assertEquals(false, $textArea->doPost());

        $textArea->setValue(self::VALUE);

        $this->assertEquals(true, self::VALUE === $textArea->getValue());
    }

    public function testRender() {
        $textArea = self::CreateTextArea();
        $htmlString = Util::GetContentRenderAsString($textArea);
        $this->assertEquals(true, strpos($htmlString, '<textarea ') !== false);
        $this->assertEquals(true, strpos($htmlString, '</textarea>') !== false);
        $this->assertEquals(true, strpos($htmlString, 'name="' . self::NAME . '"') !== false);
        $this->assertEquals(true, strpos($htmlString, 'rows="' . self::ROWS . '"') !== false);
        $this->assertEquals(false, strpos($htmlString, '<div class="error">'.self::ERROR.'</div>') !== false);
        
        $textArea->setError(self::ERROR);
        $htmlString2 = Util::GetContentRenderAsString($textArea);
        
        $this->assertEquals(true, strpos($htmlString2, '<div class="error">'.self::ERROR.'</div>') !== false);
    }
    
    private static function CreateTextArea(){
        return new TextArea(self::NAME, null, self::ROWS);
    }

}
