<?php
namespace tests\Inputs;

use tests\Util;
use Anytimestream\UI\Inputs\TextField;
use PHPUnit\Framework\TestCase;

require_once(__DIR__.'/../../vendor/autoload.php');

class TextFieldTest extends TestCase {

    const NAME = "name";
    const PLACEHOLDER = "Your name";
    const VALUE = "Norman Osaruyi";
    const ERROR = "Invalid";

    public function testProperties() {

        $textField = self::CreateTextField();
        
        $this->assertEquals(true, self::NAME === $textField->getName());

        $this->assertEquals(false, $textField->doPost());

        $textField->setValue(self::VALUE);

        $this->assertEquals(true, self::VALUE === $textField->getValue());
    }

    public function testRender() {
        $textField = self::CreateTextField();
        $htmlString = Util::GetContentRenderAsString($textField);
        $this->assertEquals(true, strpos($htmlString, '<input ') !== false);
        $this->assertEquals(true, strpos($htmlString, '/>') !== false);
        $this->assertEquals(true, strpos($htmlString, 'name="' . self::NAME . '"') !== false);
        $this->assertEquals(true, strpos($htmlString, 'placeholder="' . self::PLACEHOLDER . '"') !== false);
        $this->assertEquals(false, strpos($htmlString, '<div class="error">'.self::ERROR.'</div>') !== false);
        
        $textField->setError(self::ERROR);
        $htmlString2 = Util::GetContentRenderAsString($textField);
        
        $this->assertEquals(true, strpos($htmlString2, '<div class="error">'.self::ERROR.'</div>') !== false);
    }
    
    private static function CreateTextField(){
        return new TextField(self::NAME, null, self::PLACEHOLDER);
    }

}
