<?php
namespace tests\Inputs;

use tests\Util;
use Anytimestream\UI\Inputs\DropdownList;
use PHPUnit\Framework\TestCase;

require_once(__DIR__.'/../../vendor/autoload.php');

class DropdownListTest extends TestCase {

    const NAME = "country";
    const VALUE = "ng";
    const ERROR = "Invalid";

    public function testProperties() {

        $textArea = self::CreateDropdownList();
        
        $this->assertEquals(true, self::NAME === $textArea->getName());

        $this->assertEquals(false, $textArea->doPost());

        $textArea->setValue(self::VALUE);

        $this->assertEquals(true, self::VALUE === $textArea->getValue());
    }

    public function testRender() {
        $textArea = self::CreateDropdownList();
        $htmlString = Util::GetContentRenderAsString($textArea);
        $this->assertEquals(true, strpos($htmlString, '<select ') !== false);
        $this->assertEquals(true, strpos($htmlString, '</select>') !== false);
        $this->assertEquals(true, strpos($htmlString, '<option ') !== false);
        $this->assertEquals(true, strpos($htmlString, '</option>') !== false);
        $this->assertEquals(true, strpos($htmlString, 'name="' . self::NAME . '"') !== false);
        $this->assertEquals(true, strpos($htmlString, 'value="ng"') !== false);
        $this->assertEquals(false, strpos($htmlString, '<div class="error">'.self::ERROR.'</div>') !== false);
        
        $textArea->setError(self::ERROR);
        $htmlString2 = Util::GetContentRenderAsString($textArea);
        
        $this->assertEquals(true, strpos($htmlString2, '<div class="error">'.self::ERROR.'</div>') !== false);
    }
    
    private static function CreateDropdownList(){
        return new DropdownList(self::NAME, array('ng' => 'Nigeria', 'us' => 'United States of Ameria'));
    }

}
