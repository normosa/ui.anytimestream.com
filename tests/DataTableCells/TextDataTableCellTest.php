<?php
namespace tests\DataTables\Cells;

use Anytimestream\UI\DataTableCells\TextDataTableCell;
use PHPUnit\Framework\TestCase;
use tests\Util;

require_once(__DIR__.'/../../vendor/autoload.php');

class TextDataTableCellTest extends TestCase {
    
    public function testRender() {
        $value = "Hello World!";
        $textDataTableCell = new TextDataTableCell();
        $textDataTableCell->setValue($value);
        
        $htmlString = Util::GetContentRenderAsString($textDataTableCell);
        
        $this->assertEquals(true, strcmp($htmlString, $value) == 0);
        
    }
    
}
    