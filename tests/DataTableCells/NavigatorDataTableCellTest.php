<?php

namespace tests\DataTableCells;

use Anytimestream\UI\DataTableCells\NavigatorDataTableCell;
use PHPUnit\Framework\TestCase;
use tests\Util;

require_once(__DIR__ . '/../../vendor/autoload.php');

class NaviagtorDataTableCellTest extends TestCase {

    private $value;

    public function testRender() {

        $htmlString = $this->createNaviatorDataTableCellAndRender();

        $this->assertEquals(true, strpos($htmlString, $this->value['links'][0]['text']) !== false);
        $this->assertEquals(true, strpos($htmlString, $this->value['links'][1]['url']) !== false);
        $this->assertEquals(true, strpos($htmlString, $this->value['menu']) !== false);

        $this->assertEquals(true, strpos($htmlString, "dropdown-menu dropdown-menu-right") !== false);
        $this->assertEquals(true, strpos($htmlString, "<ul") !== false);
        $this->assertEquals(true, strpos($htmlString, "</ul>") !== false);
        $this->assertEquals(true, strpos($htmlString, "<button") !== false);
        $this->assertEquals(true, strpos($htmlString, "</button>") !== false);
        $this->assertEquals(true, strpos($htmlString, "<div") !== false);
        $this->assertEquals(true, strpos($htmlString, "</div>") !== false);
        $this->assertEquals(true, strpos($htmlString, "<li") !== false);
        $this->assertEquals(true, strpos($htmlString, "</li>") !== false);
        $this->assertEquals(true, strpos($htmlString, "<a") !== false);
        $this->assertEquals(true, strpos($htmlString, "</a>") !== false);
    }

    private function createNaviatorDataTableCellAndRender(): string {
        $this->initialize();
        
        $navigatorDataTableCell = new NavigatorDataTableCell();
        $navigatorDataTableCell->setValue($this->value);

        return Util::GetContentRenderAsString($navigatorDataTableCell);
    }

    private function initialize() {
        $this->value['menu'] = "Action";

        $this->value['links'][] = Array('url' => 'http://anytimestream.com/search', 'text' => 'Anytimestream');
        $this->value['links'][] = Array('url' => 'http://google.com/search', 'text' => 'Google');
        $this->value['links'][] = Array('url' => 'http://yahoo.com/search', 'text' => 'Yahoo');
    }

}
