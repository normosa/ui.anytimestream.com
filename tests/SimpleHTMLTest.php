<?php
namespace tests;

use Anytimestream\UI\ContainerTypes;
use Anytimestream\UI\SimpleHTML;
use PHPUnit\Framework\TestCase;

require_once(__DIR__.'/../vendor/autoload.php');

class SimpleHTMLTest extends TestCase {
    
    public function testRender() {
        $simpleHTML = new SimpleHTML();
        
        $simpleHTMLString = Util::GetContentRenderAsString($simpleHTML);
        
        $this->assertEquals(true, strpos($simpleHTMLString, '<'.ContainerTypes::HEADER) !== false);
        $this->assertEquals(true, strpos($simpleHTMLString, '</'.ContainerTypes::HEADER.'>') !== false);
        $this->assertEquals(true, strpos($simpleHTMLString, '<'.ContainerTypes::SECTION) !== false);
        $this->assertEquals(true, strpos($simpleHTMLString, '</'.ContainerTypes::SECTION.'>') !== false);
        $this->assertEquals(true, strpos($simpleHTMLString, '<'.ContainerTypes::FOOTER) !== false);
        $this->assertEquals(true, strpos($simpleHTMLString, '</'.ContainerTypes::FOOTER.'>') !== false);
        
    }
    
    public function testLayout() {
        $simpleHTML = new SimpleHTML(SimpleHTML::LAYOUTS_CONTAINER);
        
        $simpleHTMLString = Util::GetContentRenderAsString($simpleHTML);
        
        $this->assertEquals(true, strpos($simpleHTMLString, SimpleHTML::LAYOUTS_CONTAINER) !== false);       
    }
    
    public function testHeader() {
        $simpleHTML = new SimpleHTML();
        
        $header = $simpleHTML->getHeader();
        
        $this->assertEquals(true, strcmp($header->getType(), ContainerTypes::HEADER) == 0);       
    }
    
    public function testSection() {
        $simpleHTML = new SimpleHTML();
        
        $section = $simpleHTML->getSection();
        
        $this->assertEquals(true, strcmp($section->getType(), ContainerTypes::SECTION) == 0);       
    }
    
    public function testFooter() {
        $simpleHTML = new SimpleHTML();
        
        $footer = $simpleHTML->getFooter();
        
        $this->assertEquals(true, strcmp($footer->getType(), ContainerTypes::FOOTER) == 0);       
    }
}
    