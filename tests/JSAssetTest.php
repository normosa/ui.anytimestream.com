<?php

namespace tests;

use Anytimestream\UI\JSAsset;
use PHPUnit\Framework\TestCase;
use tests\Util;

require_once(__DIR__.'/../vendor/autoload.php');

class JSAssetTest extends TestCase {

    public function testRender() {

        $js = "http://ui.anytimestream.com/assets/js/jquery.js";
        
        $jsAsset = new JSAsset($js);
        
        $htmlString = Util::GetContentRenderAsString($jsAsset);
        
        $this->assertEquals(true, strpos($htmlString, $js) !== false);
        
        $this->assertEquals(true, strpos($htmlString, "text/javascript") !== false);
        
        $this->assertEquals(true, strpos($htmlString, 'language="javascript"') !== false);
        
        $this->assertEquals(true, strpos($htmlString, "<script") !== false);
    }

    
}
