<?php
namespace tests;

use Anytimestream\UI\Form;
use Anytimestream\UI\Inputs\Exception\InputNotFoundException;
use Anytimestream\UI\Inputs\TextArea;
use Anytimestream\UI\Inputs\TextField;
use Anytimestream\UI\Row;
use PHPUnit\Framework\TestCase;

require_once(__DIR__.'/../vendor/autoload.php');

class FormTest extends TestCase {
    
    public function testDatasource(){
        $datasource = array();
        $name = "Norman Osaruyi";
        $email = "norman.osaruyi@yahoo.com";
        $message = "Hello World!";
        $datasource['name']['value'] = $name;
        $datasource['email']['value'] = $email;
        $datasource['message']['value'] = $message;
        
        $form = new Form("post", "");
        $form->addContainer(new Row())->addView(new TextField("name"));
        $form->addContainer(new Row())->addView(new TextField("email"));
        $form->addContainer(new Row())->addView(new TextArea("message"));
        $form->setDataSource($datasource);
        
        $this->assertEquals(true, strcmp($name, $form->getValue("name")) == 0);
        $this->assertEquals(true, strcmp($email, $form->getValue("email")) == 0);
        $this->assertEquals(true, strcmp($message, $form->getValue("message")) == 0);
    }
    
    public function testValue(){
        $name = "Norman Osaruyi";
        $email = "norman.osaruyi@yahoo.com";
        $message = "Hello World!";
        
        $form = new Form("post", "");
        $form->addContainer(new Row())->addView(new TextField("name"));
        $form->addContainer(new Row())->addView(new TextField("email"));
        $form->addContainer(new Row())->addView(new TextArea("message"));
        
        $form->setValue("name", $name);
        $form->setValue("email", $email);
        $form->setValue("message", $message);
        
        $this->assertEquals(true, strcmp($name, $form->getValue("name")) == 0);
        $this->assertEquals(true, strcmp($email, $form->getValue("email")) == 0);
        $this->assertEquals(true, strcmp($message, $form->getValue("message")) == 0);
    }
    
    public function testInputs(){        
        $form = new Form("post", "");
        $form->addContainer(new Row())->addView(new TextField("name"));
        $form->addContainer(new Row())->addView(new TextField("email"));
        
        $this->assertEquals(true, $form->getInputCount() == 2);
        
        $form->addContainer(new Row())->addView(new TextArea("message"));
        
        $this->assertEquals(true, count($form->getInputs()) == 2);
        
        $this->assertEquals(true, count($form->getInputs(true)) == 3);
        
        $this->assertEquals(true, $form->getInputCount() == 3);
        
        try{
            $form->getInput("norman");
            $this->assertEquals(true, false);
        } catch (InputNotFoundException $ex) {
            $this->assertEquals(true, true);
        }
    }
    
    public function testErrors() {
        $error = "Inavlid";
        
        $form = new Form("post", "");
        $form->addContainer(new Row())->addView(new TextField("name"));
        $form->setError("name", $error);
        
        $this->assertEquals(true, strcmp($form->getInput("name")->getError(), $error) == 0);
        
        $form->setError("name", "");
        $this->assertEquals(true, strcmp($form->getInput("name")->getError(), "") == 0);
    }
}