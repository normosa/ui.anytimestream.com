<?php

namespace tests;

use Anytimestream\UI\CSSAsset;
use PHPUnit\Framework\TestCase;
use tests\Util;

require_once(__DIR__.'/../vendor/autoload.php');

class CSSAssetTest extends TestCase {

    public function testRender() {

        $href = "http://ui.anytimestream.com/assets/css/style.css";
        
        $cssAsset = new CSSAsset($href);
        
        $htmlString = Util::GetContentRenderAsString($cssAsset);
        
        $this->assertEquals(true, strpos($htmlString, $href) !== false);
        
        $this->assertEquals(true, strpos($htmlString, "stylesheet") !== false);
        
        $this->assertEquals(true, strpos($htmlString, "<link") !== false);
    }

    
}
